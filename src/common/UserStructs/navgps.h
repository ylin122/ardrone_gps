#pragma once
#include <stdint.h>
namespace UserStructs{
//the struct to store gps data from ardrone
struct NavGPS{

    double latitude;
    double longitude;
    double elevation;

    uint32_t data_available;

    double lat0;
    double long0;
    double lat_fused;
    double long_fused;
    uint32_t gps_state;
    float X_traj;
    float X_ref;
    float Y_traj;
    float Y_ref;

    float speed;

    uint32_t  nbsat; //Number of acquired satellites
    bool is_gps_plugged;
    float ehpe;
    float ehve;
    float vx_traj;
    float vy_traj;
};
}

