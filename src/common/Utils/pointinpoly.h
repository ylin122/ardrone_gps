#pragma once
#include "common/UserStructs/point2d.h"
#include <vector>

namespace Utils{

  bool PointInPoly(std::vector<UserStructs::point2D> vertex,double x,double y);

}//namespaces ends

