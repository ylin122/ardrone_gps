#ifndef OBSUPDATER_HPP
#define OBSUPDATER_HPP

#include "planner/UserStructs/obstacle.h"
#include <vector>
#include <string>

namespace UserTypes{
  class ObsUpdater{
    public:

      virtual bool SeeObsUpdate()= 0;
      virtual void SetObsUpdateFalse()= 0;
      virtual void UpdateObs( std::vector< UserStructs::obstacle >& obs3ds )=0;
      virtual void ReadObss( std::vector< std::string >& filenames ) = 0;
    protected:
      std::vector<bool> if_updates;

  };//class ObsUpdater ends

}//namespace ends

#endif // OBSUPDATER_HPP

