#include "sign.h"
namespace Utils{
template <typename T> int sign(T val) {
    return (T(0) < val) - (val < T(0));
}

int sign( double val ){
    return ( ( 0 < val ) - ( val < 0 ) );
}
}
