#ifndef SYSTEMTIME_H
#define SYSTEMTIME_H

#pragma once

#include <iostream>

namespace Utils
{
  void getSystemTime (std::string& time_str);
}

#endif // SYSTEMTIME_H

