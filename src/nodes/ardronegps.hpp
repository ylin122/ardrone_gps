#pragma once
//user types

//user structs and types
#include "common/UserStructs/navgps.h"
#include "common/UserStructs/waypoint.h"
#include "planner/UserTypes/statesim.hpp"
#include "planner/UserTypes/pathgenerator.hpp"
#include "othersmonitor.hpp"
#include "obsupdater/obsupdater.hpp"
#include "common/UserTypes/spacelimit.h"

//ros msgs
#include "ros/ros.h"
#include "ardrone_autonomy/navdata_gps.h"
#include "ardrone_autonomy/Navdata.h"
#include "sensor_msgs/Joy.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Int16.h"
#include "sensor_msgs/Imu.h"
#include <geometry_msgs/Twist.h>
#include <tf/LinearMath/Matrix3x3.h>

//std
#include <fstream>
#include <memory>
#include <utility>

namespace Nodes{

class ArdroneGPS
{
 public:
   ArdroneGPS();
   ~ArdroneGPS(){ };

   void SetTimeLimit( const double _t_limit ){
      this -> t_limit = _t_limit;
      path_gen.SetTimeLimit( t_limit );
   }

   //trajectory log file
   void SetLogFileName( const char* filename );
   //obstacle distance log file
   void SetObsDisFile( const char* filename );
   //avoidance waypoint log file
   void SetAvoidWpFile( const char* filename );
   //set virtual or real obstacle
   inline void SetIfRealObs( const bool _if )
   {
       if_real_obs = _if;
   }
   //set h_thres and v_thres for path generator
   inline void SetHVThres( double _h_thres,
                           double _v_thres )
   {
       this -> path_gen.SetHVThres( _h_thres, _v_thres );
       this -> predict_nav.SetHVThres( _h_thres, _v_thres );
       //h_thres = _h_thres;
       //v_thres = _v_thres;
   }
   //set space limit
   inline void SetSpaceLimit( UserTypes::SpaceLimit space_limit ){
       this -> path_gen.SetSpaceLimit( space_limit );
       this -> space_limit = space_limit;
   }
   //set goal
   inline void SetGoal( double _lat,
                        double _lon,
                        double _alt,
                        double _speed )
   {
       goal = UserStructs::Waypoint( _lat,
                                     _lon,
                                     _alt,
                                     _speed );
   }
   //set others monitor
   inline void SetOthersMonitor( std::unique_ptr< UserTypes::OthersMonitor > other_monitor )
   {
       this -> monitor = std::move( other_monitor );
   }
   //set obstacle updater
   inline void SetObsUpdater( std::unique_ptr< UserTypes::ObsUpdater > updater )
   {
       this -> obsupdater = std::move( updater ) ;
   }

   //working part
   void working();
   void working2();
   void working3( double _speed, double _hgt );
   void working4();//simple test
   void SubWork();

private:
   enum possible_case{
       NORMAL,
       PATH_READY,
       PATH_GEN,
       PATH_CHECK,
       ARRIVED
   } situ;

   struct ControlCommand
      {  //x,y,yaw, reverse
        inline ControlCommand() {roll = pitch = yaw = gaz = 0;}
        inline ControlCommand( double pitch,
                               double roll,
                               double gaz,
                               double yaw )
        {
         this->roll = roll;
         this->pitch = pitch;
         this->yaw = yaw;
         this->gaz = gaz;
        }
        double yaw, roll, pitch, gaz;
      };

   std::unique_ptr< UserTypes::ObsUpdater > obsupdater;
   std::unique_ptr< UserTypes::OthersMonitor > monitor;
   double t_limit; //time limit for path planning
   UserStructs::NavGPS nav_gps; //the gps data
   //time used to calculate vz
   ros::Time tkm1;
   ros::Time tk;
   ros::Duration elapsed_time;
   //altitude from sonar
   double zm, zm_pre, vz;
   //yaw from sonar
   double yaw_est;
   //ardrone state
   int uav_state_idx;
   int uav_state_pre;
   //if using joystick
   bool if_joy;
   bool lastR1Pressed;
   bool lastL1Pressed;
   //obstacle update
   bool if_obss_update;
   //logs
   std::ofstream traj_log;
   std::ofstream obdis_log;
   std::ofstream avoid_wp_log;
   //uav physical state
   UserTypes::StateSim uav_st;
   //path generator
   UserTypes::PathGenerator path_gen;
   //navigator for collision prediction
   UserTypes::NavigatorSim predict_nav;
   //this taken off and other taken off
   bool if_take_off;
   bool if_stable;
   bool wp_sent_done;
   //virtual or real obstacle
   bool if_real_obs;
   //the target waypoint
   double lon_s;
   double lat_s;
   double alt_s;
   //the takeoff point
   double x_take;
   double y_take;
   //thres for collision prediction
   //double h_thres;
   //double v_thres;
   int planning_num;
   //waypoints navigation
   std::vector< UserStructs::Waypoint > waypoints;
   int wp_idx;
   ros::Time t_transit;
   double dt;//dt for propagation
   //obstacles
   std::vector< UserStructs::obstacle > obstacles;
   //space limit for collision prediction
   UserTypes::SpaceLimit space_limit;
   //user types
   //UserTypes::OthersMonitor monitor;
   //goal waypoint
   UserStructs::Waypoint goal;
   //ros callback functions

   //gps
   void navdataGPSCb(const ardrone_autonomy::navdata_gpsConstPtr navdata_gpsPtr);
   //navdata
   void navdataCb( const ardrone_autonomy::NavdataConstPtr navdata_Ptr );
   //joystick
   void joyCb(const sensor_msgs::JoyConstPtr joy_msg);
   void imuCb(const sensor_msgs::ImuPtr imu_msg);

   //other trival private functions
   void PrintState();
   void SendControlToDrone( ControlCommand cmd );
   void sendLand();
   void sendTakeoff();
   void sendStop();
   void sendEmergencyStop();
   void sendFlatTrim();
   bool CheckArriveWp();
   void PubIfTakeOff();
   void PubIfStable();
   void PubState();
   void SetToDefault();
   void UpdateObss( std::vector< UserStructs::obstacle >& obss );
   int PredictColli();
   void GetVirtualObs();

   void GetCurrentSt();
   bool SendWpTest( const double _lat,
                    const double _lon,
                    const double _alt,
                    const double _vel );

   bool InsertWpTest( const double _lat,
                     const double _lon,
                     const double _alt,
                     const double _vel );

   bool SendWaypoint( const double _lat,
                      const double _lon,
                      const double _alt,
                      const double _vel );

   void CheckWpNav();

   std::string doubleToStr(const double val);

   //ros related
   //ros NodeHandle
   ros::NodeHandle nh;
   //publishers
   //command
   ros::Publisher pub_vel;//publish velocity commands
   //quad related
   ros::Publisher takeoff_pub;
   ros::Publisher land_pub;
   ros::Publisher emergency_pub;
   ros::Publisher pub_if_take_off;
   ros::Publisher pub_if_stable;
   ros::Publisher pub_state;
   //subscribers
   ros::Subscriber joy_sub;//subscribe to joystick command
   ros::Subscriber nav_sub;//subscribe to navdateCb
   ros::Subscriber gps_sub;//subscribe to gps data
   ros::Subscriber imu_sub;//subscribe to imu data
   //service
   ros::ServiceClient client_setgpstarget;
   ros::ServiceClient client_setautoflight;
   ros::ServiceClient flattrim_srv;
};

}


