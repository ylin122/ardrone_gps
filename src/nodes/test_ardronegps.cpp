#include "ardronegps.hpp"
#include "common/Utils/yclogger.h"
#include "common/Utils/findpath.h"
#include "common/Utils/systemtime.h"
#include "nodes/obsupdater/obsrealupdater.hpp"
#include "nodes/obsupdater/obsvirtualupdater.hpp"
#include "ros/ros.h"

int main( int argc, char** argv )
{
    ros::init( argc, argv, "ArdroneGPS" );
    Nodes::ArdroneGPS ardrone_gps;

    Utils::LogConfigurator myconfigurator("log4cxx_ArdroneGPS.properties","log for ArdroneGPS");

    std::string str_time;
    Utils::getSystemTime( str_time );

    //traj file
    std::string traj_file = Utils::FindPath() + "records/traj_log_" + str_time + ".txt";
    ardrone_gps.SetLogFileName( traj_file.c_str() );

    //obstacle distance file
    std::string obdis_file = Utils::FindPath() + "records/obs_log_" + str_time + ".txt";
    ardrone_gps.SetObsDisFile( obdis_file.c_str() );

    //avoidance waypoint file
    std::string avoid_wp_file = Utils::FindPath() +
"records/avoid_wp_log_" + str_time + ".txt";
    ardrone_gps.SetAvoidWpFile( avoid_wp_file.c_str() );

    //for planning
    ardrone_gps.SetTimeLimit( 0.5 );
    ardrone_gps.SetIfRealObs( false );
    ardrone_gps.SetHVThres( 2, 0.5 );
    //space limit
    UserTypes::SpaceLimit spacelimit( 2.5, 0.5 );
    spacelimit.LoadGeoFence( (Utils::FindPath()+ "records/geo_fence_narrow.txt").c_str() );
    ardrone_gps.SetSpaceLimit( spacelimit );
    //goal
    double lat_s = 33.4102431;
    double lon_s = -111.89398441;
    /*
    double x_s, y_s;
    Utils::ToUTM( lon_s, lat_s, x_s, y_s );
    x_s -= 2;
    Utils::FromUTM( x_s, y_s, lon_s, lat_s );
    */
    double alt_s = 1.5;
    double spd_d = 1.0;
    ardrone_gps.SetGoal( lat_s, lon_s, alt_s, spd_d );
    //others monitor
    //for real obstacle
    auto monitor = std::unique_ptr< UserTypes::OthersMonitor > ( new UserTypes::OthersMonitor( 0, 1 ) );
    ardrone_gps.SetOthersMonitor( std::move( monitor ) );
    //for virtual obstacle
    //ardrone_gps.SetOthersMonitor( UserTypes::OthersMonitor( 0, 0 ) );
    //obstacle updater
    auto obs_updater = std::unique_ptr< UserTypes::ObsUpdater > ( new UserTypes::ObsRealUpdater( 0, 1 ) );

    /*
    std::vector< std::string > filenames;
    filenames.push_back( Utils::FindPath()+"records/obs_traj0.txt");
    auto obs_updater = std::unique_ptr< UserTypes::ObsUpdater > ( new UserTypes::ObsVirtualUpdater( filenames ) );
    */
    ardrone_gps.SetObsUpdater( std::move( obs_updater ) );
    ardrone_gps.working2();

}

