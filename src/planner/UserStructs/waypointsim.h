#ifndef WAYPOINTSIM_H
#define WAYPOINTSIM_H
namespace UserStructs {
   struct WaypointSim{
       //UTM
       double x;
       double y;
       double alt;
       double speed;
       //error for arrive
       double x_err;
       double y_err;
       double alt_err;

       WaypointSim():x(0),
                     y(0),
                     alt(0),
                     x_err(0),
                     y_err(0),
                     alt_err(0),
                     speed(0)
       {

       }

       WaypointSim(double _x,
                   double _y,
                   double _alt,
                   double _x_err,
                   double _y_err,
                   double _alt_err,
                   double _speed ):
                   x( _x ),
                   y( _y ),
                   alt( _alt ),
                   x_err( _x_err ),
                   y_err( _y_err ),
                   alt_err( _alt_err ),
                   speed( _speed + 0.5 )
       {

       }

   };
}
#endif // WAYPOINTSIM_H

