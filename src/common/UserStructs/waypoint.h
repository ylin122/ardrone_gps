#ifndef WAYPOINT_H
#define WAYPOINT_H
namespace UserStructs{
   struct Waypoint{
       double lat;
       double lon;
       double alt;
       double speed;

       Waypoint():lat( 0. ),
                  lon( 0. ),
                  alt( 0. ),
                  speed( 0. )
       {

       }

       Waypoint( const double _lat,
                 const double _lon,
                 const double _alt,
                 const double _speed ):
               lat( _lat ),
               lon( _lon ),
               alt( _alt ),
               speed( _speed )
       {

       }

   };
}
#endif // WAYPOINT_H

