#pragma once
#include "statesim.hpp"
#include "planner/UserStructs/waypointsim.h"
#include "planner/UserStructs/obstacle.h"
#include "common/UserTypes/spacelimit.h"
#include <fstream>

namespace UserTypes {
class NavigatorSim{
   public:
     NavigatorSim();
     ~NavigatorSim(){ }

     void PropagateWp( const StateSim current_st,
                       const UserStructs::WaypointSim wp,
                       const double dt);

     bool PropWpCheck( const StateSim current_st,
                       const UserStructs::WaypointSim wp,
                       const double dt,
                       std::vector< UserStructs::obstacle > obstacles,
                       UserTypes::SpaceLimit space_limit );

     void SetProLogName( const char* filename );

     inline void SetIfLogProg( const bool _if ){
         if_log_prog = _if;
     }

     inline void SetIfNavRec( const bool _if ){
         if_navi_rec = _if;
     }

     inline void SetHVThres( const double _h_thres,
                             const double _v_thres )
     {
         h_thres = _h_thres;
         v_thres = _v_thres;
     }

     inline void GetNavRec( std::vector< UserTypes::StateSim >& states_rec )
     {
         states_rec = this -> navi_rec;
     }

     inline StateSim GetStNow(){ return st_now; }

   private:
     std::ofstream prog_log;
     bool if_log_prog;
     std::vector< UserTypes::StateSim > navi_rec;
     //the final state
     UserTypes::StateSim st_now;
     bool if_navi_rec;
     //for collision checking
     double h_thres;
     double v_thres;

};
}

