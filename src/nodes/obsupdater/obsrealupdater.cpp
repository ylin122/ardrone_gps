#include "obsrealupdater.hpp"
#include "common/Utils/gettimenow.h"

namespace UserTypes{

  ObsRealUpdater::ObsRealUpdater(int _one,int _total):one(_one),total(_total)
  {
    for(int i = 0; i != total + 1; ++i )
    {
      if( i == one ){
          continue;
      }
      int idx = i;
      std::ostringstream convert;
      convert << idx;
      std::string idx_str = convert.str();
      //subscriber to other vehicles' states
      ros::Subscriber sub_st = nh.subscribe< ardrone_gps::ArdroneState>(std::string("/drone")+idx_str+"/quad_state",1,boost::bind( &ObsRealUpdater::state_obCb, this, _1, idx ) );
      state_subs.push_back(sub_st);
      if_updates.push_back(false);
      obstacles.push_back( UserStructs::obstacle() );
    }//for ends

  }//ObsRealUpdater constructor ends

  void ObsRealUpdater::state_obCb(const ardrone_gps::ArdroneState::ConstPtr& msg,int _idx)
  {
    UserStructs::obstacle obs( Utils::GetTimeNow(),
                               msg -> x,
                               msg -> y,
                               msg -> z,
                               msg -> yaw,
                               msg -> speed,
                               msg -> vz );
    //push back states
    if( _idx > one ){
        --_idx;
    }
    obstacles[_idx] = obs;
    if_updates[_idx] = true;
  }//state_obCb ends

  bool ObsRealUpdater::SeeObsUpdate()
  {
    for(int i = 0; i != total; ++i){
      if( !if_updates[i] ) {
          return false;
      }
    }
    return true;
  }//SeeObsUpdate() ends

  void ObsRealUpdater::SetObsUpdateFalse()
  {
    for(int i=0;i!=total;++i){
      if_updates[i]= false;
    }
  }//SetObsUpdateFalse ends

  void ObsRealUpdater::UpdateObs(std::vector<UserStructs::obstacle>& obs3ds )
  {
    if( SeeObsUpdate() ){
        obs3ds = obstacles;
    }
    else{
        obs3ds = std::vector<UserStructs::obstacle>();
    }
    SetObsUpdateFalse();
  }//UpdateObs ends

  void ObsRealUpdater::ReadObss( std::vector< std::string >& filenames )
  {
     //just showing implementation
  }
}//namespace ends
