#Setup
. setup.sh 192.168.0.1
NUMID_DRONE=1
DRONE_IP=192.168.1.1

gnome-terminal  \
	--tab --title "ardrone_driver"	--command "bash -c \"
		roslaunch ./launch/ardrone_driver.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_ip:=$DRONE_IP;
		exec bash\""  \
	--tab --title "joystick"  --command "bash -c \"
		roslaunch ./launch/joy_node.launch --wait drone_id_namespace:=drone$NUMID_DRONE;
		exec bash\""  \
	--tab --title "test_invadergps"  --command "bash -c \"
		roslaunch ./launch/exe_invader.launch --wait drone_id_namespace:=drone$NUMID_DRONE;
		exec bash\""  \
