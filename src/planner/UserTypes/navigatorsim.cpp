#include "navigatorsim.hpp"
#include "common/Utils/yclogger.h"
#include <iomanip>
#include <iostream>
#include <cmath>

namespace{
Utils::LoggerPtr s_logger(Utils::getLogger("ardrone_gps.NavigatorSim.YcLogger"));
}

namespace UserTypes{

   NavigatorSim::NavigatorSim():if_log_prog( false ),
                                if_navi_rec( false ),
                                h_thres( 2.0 ),
                                v_thres( 0.5 )
   {

   }

   void NavigatorSim::PropagateWp(const StateSim current_st,
                                  const UserStructs::WaypointSim wp,
                                  const double dt)
   {

       st_now = current_st;
       while( !st_now.SeeArrive( wp ) )
       {
           if( if_log_prog)
           {
               prog_log << std::setprecision(6) << std::fixed
                       << st_now.t << ' '
                       << st_now.lat << ' '
                       << st_now.lon << ' '
                       << st_now.x << ' '
                       << st_now.y << ' '
                       << st_now.alt << ' '
                       << st_now.speed << ' '
                       << st_now.vz << ' '
                       << st_now.yaw << '\n';
           }

           st_now = st_now.WpUpdate( wp, dt );

       }
   }

   bool NavigatorSim::PropWpCheck(const StateSim current_st,
                                  const UserStructs::WaypointSim wp,
                                  const double dt,
                                  std::vector<UserStructs::obstacle> obstacles,
                                  SpaceLimit space_limit)
   {
       st_now = current_st;
       int N_rec = 5;
       int count = 0;
       navi_rec.clear();
       double dis = std::sqrt( std::pow( current_st.x - wp.x, 2 )
                             + std::pow( current_st.y - wp.y, 2 ) );
       double tra_len = 0;
       while( !st_now.SeeArrive( wp ) )
       {
           if( if_navi_rec && count % N_rec == 0 ){
             navi_rec.push_back( st_now );
           }
           /*
           if( !st_now.CheckSpace( space_limit )
             ||st_now.CheckObss( obstacles, h_thres, v_thres ) )
           {
               return true;
           }
           */
           bool if_out_space = ! st_now.CheckSpace( space_limit );
           bool if_obs_colli = st_now.CheckObss( obstacles, h_thres, v_thres );
           /*
           if( if_out_space ){
               YCLOG( s_logger, LL_DEBUG, "out space" );
               YCLOG( s_logger, LL_DEBUG, "z:" << st_now.alt );
           }*/
           if( if_obs_colli ){
               //YCLOG( s_logger, LL_DEBUG, "obs colli" );
           }

           if( if_obs_colli || if_out_space ){
               return true;
           }
           st_now = st_now.WpUpdate( wp, dt );
           tra_len += st_now.speed * dt;

           if( tra_len > 1.5 * dis ){
               //YCLOG( s_logger, LL_DEBUG, "not reachable due to geometry" );
               break;
           }

           ++ count;
       }
       return false;
   }

   void NavigatorSim::SetProLogName( const char* filename )
   {
       try
       {
           prog_log.exceptions ( std::ofstream::failbit | std::ofstream::badbit );
           prog_log.open(filename,std::ofstream::out
                         | std::ofstream::in
                         | std::ofstream::trunc);
       }
       catch (std::ofstream::failure& e) {
           std::cerr << "Exception opening/reading file"
                     << e.what()
                     << std::endl;
       }
   }

}
