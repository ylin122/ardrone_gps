#Setup
. setup.sh
NUMID_DRONE=0
DRONE_IP=192.168.1.1

gnome-terminal  \
	--tab --title "ardrone_driver"	--command "bash -c \"
                env sleep 10s ;
		roslaunch ./launch/ardrone_driver.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_ip:=$DRONE_IP;
		exec bash\""  \
	--tab --title "joystick"  --command "bash -c \"
		roslaunch ./launch/joy_node.launch --wait drone_id_namespace:=drone$NUMID_DRONE;
		exec bash\""  \
	--tab --title "test_ardronegps"  --command "bash -c \"
                env sleep 10s ;
		roslaunch ./launch/exe_host.launch --wait drone_id_namespace:=drone$NUMID_DRONE;
		exec bash\""  \
