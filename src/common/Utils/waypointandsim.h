#ifndef WAYPOINTANDSIM_H
#define WAYPOINTANDSIM_H

#include "planner/UserStructs/waypointsim.h"
#include "common/UserStructs/waypoint.h"

namespace Utils{

      UserStructs::WaypointSim WpToSim( UserStructs::Waypoint wp );
      UserStructs::Waypoint WpFromSim( UserStructs::WaypointSim wp_sim );

}

#endif // WAYPOINTANDSIM_H

