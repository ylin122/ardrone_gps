gnome-terminal \
	--tab --title "roscore" --command "bash -c \"
				roscore; 
				exec bash\""  \
	--tab --title "ardrone_driver" --command "bash -c \"
				env sleep 3s ;
				rosrun ardrone_autonomy ardrone_driver _realtime_navdata:=True _navdata_demo:=1 _enable_navdata_gps:=True; 
				exec bash\""  \
	--tab --title "joystick" --command "bash -c \"
				env sleep 3s ;
                                rosparam set joy_node/dev "/dev/input/js1"
				rosrun joy joy_node;
				exec bash\""  \
	--tab --title "ardrone_gps" --command "bash -c \"
	                        env sleep 3s;
	                        rosrun ardrone_gps test_ardronegps; 
	                        exec bash\""  \
