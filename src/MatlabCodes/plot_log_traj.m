close all;
clear all;
clc;

f_traj =fopen('../../records/traj_log17.txt','r');

if f_traj == -1
    error('File traj_log.txt could not be opened, check name or path.')
end

traj_line= fgetl(f_traj);
virtual_traj = [];
cal_traj = [];

count = 1;
% x0 = 416878.740330;
% y0 = 3697125.599913;
x0 = 0, y0 = 0;
while ischar(traj_line)
%1440980152.890608 33.410139 -111.893931 416879.891995 3697113.526170 360.340000 2.190000 0.229869 -0.079203 23.740421 23.740421 1.087235 1.087235 1.983929 0.253034 3
    log_traj = textscan(traj_line,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f');
    state_id = log_traj{16};

    if state_id == 3
    
        t = log_traj{1};

        if count == 1
           t0 = t;
        end
        t = t - t0;

        lat = log_traj{2};
        lon = log_traj{3};

        x = log_traj{4};
        y = log_traj{5};
        if count == 1
           x0 = x;
           y0 = y;
        end
%         x0 = 0;
%         y0 = 0;
         x = x - x0;
         y = y - y0;

        alt = log_traj{6};
        speed = log_traj{7};
        vz = log_traj{8};
        yaw = log_traj{9} * 180. / pi;
        X_ref = log_traj{10};
        X_traj = log_traj{11};
        Y_ref = log_traj{12};
        Y_traj = log_traj{13};
        vx_traj = log_traj{14};
        vy_traj = log_traj{15};

        vv = sqrt( vx_traj * vx_traj + vy_traj * vy_traj );
        v_yaw = atan2( vy_traj, vx_traj ) * 180. / pi;

        virtual_traj = [ virtual_traj; [t,lat,lon,x,y,alt,speed,vz,yaw,X_ref,X_traj,Y_ref,Y_traj,vx_traj,vy_traj,state_id] ];
        cal_traj = [ cal_traj; [t,vv,v_yaw] ];
        count = count + 1;
    end
    traj_line = fgetl( f_traj );
    
end

%propagate model
% model_traj = [];
% size2 = size( cal_traj, 1 );
% vv = cal_traj( size2, 2 );
% for i = 1:size(virtual_traj,1)
%     t = virtual_traj(i,1);
%     if i == 1
% %        x0 = virtual_traj(i,4);
% %        y0 = virtual_traj(i,5);
%        x = x0;
%        y = y0;
%        %model_traj = [ model_traj; [ t, x, y0 ] ];
%     else
%        dt = t - virtual_traj(i-1,1);
%        yaw = 90 - cal_traj(i,3);
%        %yaw = virtual_traj(i,9);
%        vv = virtual_traj(i,7);
%        x = x + vv * cosd( yaw ) * dt;
%        y = y + vv * sind( yaw ) * dt;
%     end
%     model_traj = [ model_traj; [ t, x, y ] ];
% end

%another model
f_traj =fopen('/home/yucong/catkin_ws/devel/lib/ardrone_gps/sim_traj17.txt','r');
%f_traj =fopen('/home/yucong/catkin_ws/src/ardrone_gps/records/obs_traj.txt','r');

if f_traj == -1
    error('File traj_log.txt could not be opened, check name or path.')
end

traj_line= fgetl(f_traj);
sim_traj = [];

count = 1;
% x0 = 416878.740330;
% y0 = 3697125.599913;
while ischar(traj_line)
%1440980152.890608 33.410139 -111.893931 416879.891995 3697113.526170 360.340000 2.190000 0.229869 -0.079203 23.740421 23.740421 1.087235 1.087235 1.983929 0.253034 3
    log_traj = textscan(traj_line,'%f %f %f %f %f %f %f %f %f');
    t = log_traj{1};
    
    if count == 1
       t0 = t;
    end
    t = t - t0;
    
    lat = log_traj{2};
    lon = log_traj{3};
    
    x = log_traj{4};
    y = log_traj{5};
    if count == 1
       x0 = x;
       y0 = y;
    end
%     x0 = 0;
%     y0 = 0;
    x = x - x0;
    y = y - y0;
    
    alt = log_traj{6};
    speed = log_traj{7};
    vz = log_traj{8};
    yaw = log_traj{9} * 180. / pi - 90;
    
    sim_traj = [ sim_traj; [t,lat,lon,x,y,alt,speed,vz,yaw] ]; 
    
    traj_line = fgetl( f_traj );
    count = count + 1;
    if count == 3000
       break;
    end
end

%plot X_traj, Y_traj


h = figure;
hold on;
axis auto;
grid on;
%title('x');
m = size( virtual_traj, 1 );
% plot( virtual_traj(:,1), virtual_traj(:,13), 'r*' );
% plot( virtual_traj(:,1), virtual_traj(:,12), 'r-' );
plot( virtual_traj(1:m-40,1), virtual_traj(41:m,4), 'b*' );
m1 = size( sim_traj, 1 );
plot( sim_traj(1:m1-19,1), sim_traj(1:m1-19,4), 'b-' );
%plot( virtual_traj(:,1), model_traj(:,2), 'm*' );
set(gca,'fontsize',20);
xlabel('time(s)');
ylabel('x(m)');
legend('actual','model','Location','northwest');
legend boxoff;
saveas(h,'x_comp','fig');

h=figure;
hold on;
axis auto;
grid on;
%title('y');
% plot( virtual_traj(:,1), virtual_traj(:,11), 'r*' );
% plot( virtual_traj(:,1), virtual_traj(:,10), 'r-' );
plot( virtual_traj(1:62,1), virtual_traj(1:62,5), 'b*' );
plot( sim_traj(1:62,1), sim_traj(1:62,5), 'b-' );
%plot( virtual_traj(:,1), model_traj(:,3), 'm*' );
set(gca,'fontsize',20);
xlabel('time(s)');
ylabel('y(m)');
saveas(h,'y_comp','fig');

dis = zeros(62,1);
for i = 1:62
   dis(i) = sqrt( ( virtual_traj(i,5) - sim_traj(i,5) )^2  + ( virtual_traj(i+40,4) - sim_traj(i,4) )^2 ); 
end
h=figure;
hold on;
axis auto;
grid on;
plot( virtual_traj(1:62,1), dis, 'k-*' );
set(gca,'fontsize',20);
xlabel('time(s)');
ylabel('distance(m)');
saveas(h,'dis_xy','fig');

% %plot yaw
% figure;
% hold on;
% axis auto;
% grid on;
% title('yaw');
% plot( virtual_traj(:,1), virtual_traj(:,9), 'k*' );
% plot( sim_traj(:,1), sim_traj(:,9), 'r*' );
%plot( virtual_traj(:,1), cal_traj(:,3), 'k-' );
% 
% %plot v
% figure;
% hold on;
% axis auto;
% grid on;
% title('v_traj');
% plot( virtual_traj(:,1), virtual_traj(:,14), 'r*' );
% plot( virtual_traj(:,1), virtual_traj(:,15), 'b*' );
% plot( virtual_traj(:,1), cal_traj(:,2), 'k*' );
% 
% %plot state_idx
% figure;
% hold on;
% axis auto;
% grid on;
% title('state_idx');
% plot( virtual_traj(:,1), virtual_traj(:,16), 'r*' );
% 
% %plot x
% figure;
% hold on;
% axis auto;
% grid on;
% title('x');
% plot( virtual_traj(:,1), virtual_traj(:,4), 'b*' );
% 
% figure;
% hold on;
% axis auto;
% grid on;
% title('y');
% plot( virtual_traj(:,1), virtual_traj(:,5), 'r*' );
% 
% %plot speed
% figure;
% hold on;
% axis auto;
% grid on;
% title('speed');
% plot( virtual_traj(:,1), virtual_traj(:,7), 'r*' );
% plot( sim_traj(:,1), sim_traj(:,7), 'r-' );
% 
% %plot vz
% figure;
% hold on;
% axis auto;
% grid on;
% title('vz');
% plot( virtual_traj(:,1), virtual_traj(:,8), 'r*' );
% 
% %plot alt
% figure;
% hold on;
% axis auto;
% grid on;
% title('alt');
% plot( sim_traj(:,1), sim_traj(:,6), 'r*' );
% 
% %plot traj
% figure;
% hold on;
% axis equal;
% grid on;
% title('actual traj');
% plot( virtual_traj(:,4), virtual_traj(:,5), 'r*' );

%compare height
f_traj =fopen('../../records/traj_log_20150912-103649.txt','r');

if f_traj == -1
    error('File traj_log.txt could not be opened, check name or path.')
end

traj_line= fgetl(f_traj);
h_traj = [];
count = 0;
while ischar(traj_line)
%1440980152.890608 33.410139 -111.893931 416879.891995 3697113.526170 360.340000 2.190000 0.229869 -0.079203 23.740421 23.740421 1.087235 1.087235 1.983929 0.253034 3
    log_traj = textscan(traj_line,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f');
    t = log_traj{1};
    alt = log_traj{6};
    state_id = log_traj{16};

    if state_id == 3
        if count == 0
            t0 = t;
        end
        t = t - t0; 
        h_traj = [ h_traj; [t, alt] ];
        count = count + 1;
    end
    traj_line= fgetl(f_traj);
    
end

h=figure;
hold on;
axis auto;
grid on;
%title('height');
ylim([1 1.8]);
plot( h_traj(1:62,1), h_traj(1:62,2), 'b*' );
plot( sim_traj(1:62,1), sim_traj(1:62,6), 'b-' );
set(gca,'fontsize',20);
xlabel('time(s)');
ylabel('height(m)');
saveas(h,'h_comp','fig');