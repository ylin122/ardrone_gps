#ifndef OBSMONITOR_HPP
#define OBSMONITOR_HPP

#include "ros/ros.h"
#include "std_msgs/Bool.h"

namespace UserTypes
{
  //to monitor other drones states
  class OthersMonitor{
    public:
     OthersMonitor(int _one,int _total);

     bool ifOthersTakeOff();
     bool ifSomeTakeOff(int _idx);
     bool ifOthersStable();
    private:
     int one; //the index of this drone
     int total; //the total number of other drones
     //ros NodeHandle
     ros::NodeHandle nh;
     //subscribers to see if other vehicles starts to take off
     std::vector<ros::Subscriber> take_off_subs;
     //ros::Subscriber off_sub;
     //subscribers to see if other vehicles already finished taking off
     std::vector<ros::Subscriber> stable_subs;
     //ros::Subscriber stable_sub;
     //flags of take off
     std::vector<bool> if_take_offs;
     //flags of stable
     std::vector<bool> if_stables;
     //callback functions
     void offCb(const std_msgs::Bool::ConstPtr& msg, int _idx);
     void stableCb(const std_msgs::Bool::ConstPtr& msg, int _idx);
  };//class OthersMonitor ends

}

#endif // OBSMONITOR_HPP

