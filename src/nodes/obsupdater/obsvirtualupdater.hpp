#ifndef OBSVIRTUALUPDATER_HPP
#define OBSVIRTUALUPDATER_HPP

#include "obsupdater.hpp"
#include <vector>
#include <string>
#include "ardrone_gps/ArdroneState.h"

namespace UserTypes{
  class ObsVirtualUpdater:public ObsUpdater{
    public:
      ObsVirtualUpdater(std::vector<std::string> filenames);
      ~ObsVirtualUpdater(){};
      inline bool SeeObsUpdate(){ return true; }
      inline void SetObsUpdateFalse(){/*do nothing*/}
      void UpdateObs(std::vector<UserStructs::obstacle>& obs3ds );
      void ReadObss( std::vector< std::string >& filenames );
    private:
      std::vector< std::vector<UserStructs::obstacle> > vec_obss;
      int obs_count;
  };//class ends

}//namespace ends

#endif // OBSVIRTUALUPDATER_HPP

