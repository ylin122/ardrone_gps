#ifndef SPACELIMIT_H
#define SPACELIMIT_H

#include "common/Utils/utmtransform.h"
#include "common/UserStructs/point2d.h"
#include <vector>

namespace UserTypes{
   class SpaceLimit{
     private:
       //height upper limit
       double h_upper;
       double h_lower;
       //vertex for the geofencing polygon
       std::vector< UserStructs::point2D > vertex;
     public:
       //constructor
       SpaceLimit();
       ~SpaceLimit(){ }

       SpaceLimit( double _h_upper, double _h_lower );

       bool TellIn( double x, double y, double z );

       void LoadGeoFence( const char* filename );
   };
}

#endif // SPACELIMIT_H

