#ifndef OBSREALUPDATER_HPP
#define OBSREALUPDATER_HPP

#include "obsupdater.hpp"
#include <vector>
#include "ros/ros.h"
#include "ardrone_gps/ArdroneState.h"

namespace UserTypes{
  class ObsRealUpdater : public ObsUpdater{
     public:
        ObsRealUpdater(int one,int total);
        bool SeeObsUpdate();
        void SetObsUpdateFalse();
        void UpdateObs( std::vector< UserStructs::obstacle >& obs3ds );
        void ReadObss( std::vector< std::string >& filenames );
     private:
        int one,total;
        ros::NodeHandle nh;
        std::vector<ros::Subscriber> state_subs;
        std::vector< UserStructs::obstacle > obstacles;
        void state_obCb(const ardrone_gps::ArdroneState::ConstPtr& msg,int _idx);

  };//class ends

}

#endif // OBSREALUPDATER_HPP

