#include "waypointandsim.h"
#include "common/Utils/utmtransform.h"
namespace Utils{

  UserStructs::WaypointSim WpToSim( UserStructs::Waypoint wp )
  {
     double x_s, y_s;
     Utils::ToUTM( wp.lon, wp.lat, x_s, y_s );
     return UserStructs::WaypointSim( x_s,
                                      y_s,
                                      wp.alt,
                                      wp.speed,
                                      2,
                                      2,
                                      0.2 );
  }

  UserStructs::Waypoint WpFromSim( UserStructs::WaypointSim wp_sim )
  {
     double lat_s, lon_s;
     Utils::FromUTM( wp_sim.x, wp_sim.y, lon_s, lat_s );
     return UserStructs::Waypoint( lat_s,
                                   lon_s,
                                   wp_sim.alt,
                                   wp_sim.speed );
  }

}
