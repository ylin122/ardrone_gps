#include "polesampler.hpp"
#include <cmath>
#include <boost/random.hpp>

namespace UserTypes{

   PoleSampler::PoleSampler()
   {

   }

   void PoleSampler::SetParams( const UserTypes::StateSim st_current,
                                const UserStructs::WaypointSim wp_goal,
                                const double sig_ga)
   {
      double x_goal = wp_goal.x;
      double y_goal = wp_goal.y;
      double z_goal = wp_goal.alt;

      double x_start = st_current.x;
      double y_start = st_current.y;
      double z_start = st_current.alt;

      double Dx = x_goal - x_start;
      double Dy = y_goal - y_start;
      double Dz = z_goal - z_start;

      theta0 = atan2( Dy, Dx );
      r0 = sqrt( Dx*Dx + Dy*Dy + Dz*Dz );

      x0 = x_start;
      y0 = y_start;
      z0 = z_start;
      sigma_r = 0.5 * r0;
      ga0 = 0.;
      sigma_ga = sig_ga;
   }

   void PoleSampler::GetSample( double &x_a,
                                double &y_a,
                                double &z_a )
   {
       boost::mt19937 generator;
       static unsigned int seed = 0;
       generator.seed(static_cast<unsigned int>(std::time(0))+(++seed));

       //std::cout<<"sigma_r= "<< sigma_r <<std::endl;
       boost::normal_distribution<> r_distribution(r0,sigma_r);
       boost::variate_generator<boost::mt19937&,boost::normal_distribution<> > r_nor(generator, r_distribution);
       double r= r_nor();

       //UASLOG(s_logger,LL_DEBUG,"sample theta0:"<< theta0*180./M_PI);
       double alpha= M_PI / 3;
       double left = 0;
       double right = 1.;
       boost::uniform_real<> the_uniform(theta0 - right * alpha, theta0 + left * alpha);
       boost::variate_generator<boost::mt19937&,boost::uniform_real<> > the_nor(generator, the_uniform);
       double theta= the_nor();

       boost::normal_distribution<> ga_distribution(ga0, sigma_ga);
       boost::variate_generator<boost::mt19937&,boost::normal_distribution<> > ga_nor(generator, ga_distribution);
       double ga= ga_nor();

       if( r < 6 ){
           r = 6;
       }
       x_a = x0 + r * cos(theta);
       y_a = y0 + r * sin(theta);
       z_a = z0;
   }

}
