#ifndef OBSTACLE_H
#define OBSTACLE_H
#include <cmath>
namespace UserStructs{
    struct obstacle{
        double t;
        double x;
        double y;
        double z;
        double heading;
        double speed;
        double vz;

        obstacle():t(0.),
                   x(0.),
                   y(0.),
                   z(0.),
                   heading(0.),
                   speed(0.),
                   vz(0.)
        {

        }

        obstacle( double _t,
                  double _x,
                  double _y,
                  double _z,
                  double _heading,
                  double _speed,
                  double _vz ):
                t( _t ),
                x( _x ),
                y( _y ),
                z( _z ),
                heading( _heading ),
                speed( _speed ),
                vz( _vz )
        {

        }

        obstacle extropolate( double dt )
        {
            obstacle obs;
            obs.x = x + speed * cos( heading ) * dt;
            obs.y = y + speed * sin( heading ) * dt;
            obs.z = z + vz * dt;
            obs.t = t + dt;
            obs.heading = heading;
            obs.speed = speed;
            obs.vz = vz;

            return obs;
        }

    };
}

#endif // OBSTACLE_H

