close all;
clear all;
clc;

f_traj =fopen('/home/yucong/catkin_ws/devel/lib/ardrone_gps/path_log.txt','r');

if f_traj == -1
    error('File path_log.txt could not be opened, check name or path.')
end

traj_line= fgetl(f_traj);
sim_traj = [];

count = 1;

while ischar(traj_line)
%1440980152.890608 33.410139 -111.893931 416879.891995 3697113.526170 360.340000 2.190000 0.229869 -0.079203 23.740421 23.740421 1.087235 1.087235 1.983929 0.253034 3
    log_traj = textscan(traj_line,'%f %f %f');
    
    x = log_traj{1};
    y = log_traj{2};
    if count == 1
       x0 = x;
       y0 = y;
    end
    x = x - x0;
    y = y - y0;
    alt = log_traj{3};
    
    sim_traj = [ sim_traj; [x,y,alt] ]; 
    
    traj_line = fgetl( f_traj );
    count = count + 1;
end

figure;
hold on;
axis equal;
grid on;
title('path');
%view( 3 );
plot3( sim_traj(:,1), sim_traj(:,2), sim_traj(:,3), 'b*' );

