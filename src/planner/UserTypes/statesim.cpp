﻿#include "statesim.hpp"
#include "common/Utils/utmtransform.h"
#include "common/Utils/sign.h"
#include <cmath>
#include <iostream>

namespace UserTypes{

   StateSim::StateSim():t(0),
                        x(0),
                        y(0),
                        alt(0),
                        lat(0),
                        lon(0),
                        speed(0),
                        vz(0),
                        yaw(0),
                        X_traj(0),
                        X_ref(0),
                        Y_traj(0),
                        Y_ref(0),
                        vx_traj(0),
                        vy_traj(0)
   {

   }

   StateSim::StateSim( double _t,
                       double _x,
                       double _y,
                       double _alt,
                       double _lat,
                       double _lon,
                       double _speed,
                       double _vz,
                       double _yaw ):
       t( _t ),
       x( _x ),
       y( _y ),
       alt( _alt ),
       lat( _lat ),
       lon( _lon ),
       speed( _speed ),
       vz( _vz ),
       yaw( _yaw )
   {

   }

   StateSim::StateSim( double _t,
                       double _x,
                       double _y,
                       double _alt,
                       double _lat,
                       double _lon,
                       double _speed,
                       double _vz,
                       double _yaw,
                       double _X_traj,
                       double _X_ref,
                       double _Y_traj,
                       double _Y_ref,
                       double _vx_traj,
                       double _vy_traj):
                     t( _t ),
                     x( _x ),
                     y( _y ),
                     alt( _alt ),
                     lat( _lat ),
                     lon( _lon ),
                     speed( _speed ),
                     vz( _vz ),
                     yaw( _yaw ),
                     X_traj(_X_traj),
                     X_ref(_X_ref),
                     Y_traj(_Y_traj),
                     Y_ref(_Y_ref),
                     vx_traj(_vx_traj),
                     vy_traj(_vy_traj)
   {

   }

   void StateSim::GetUTM()
   {
        Utils::ToUTM(lon,lat,x,y);
   }

   void StateSim::GetGCS()
   {
        Utils::FromUTM(x,y,lon,lat);
   }

   void StateSim::CheckConvert()
   {
       if(lat==0 && lon==0){
           GetGCS();
       }
       if(x==0 && y==0){
           GetUTM();
       }
   }

   StateSim StateSim::WpUpdate( const UserStructs::WaypointSim wp,
                                double dt )
   {
       //yaw
       double hd = atan2( wp.y - y, wp.x - x );
       double max_yaw_rate = 60. / 180 * M_PI;
       double k = 0.1;
       double d_yaw =  max_yaw_rate * dt > std::abs( k * ( hd - yaw ) ) ? k * ( hd - yaw ) : Utils::sign( hd - yaw ) * max_yaw_rate * dt;

       yaw += d_yaw;

       //speed
       double max_accel = 1.0;
       //std::cout << "wp.speed:" << wp.speed << '\n';
       double d_speed = max_accel * dt > std::abs( k * ( wp.speed - speed ) ) ? k * ( wp.speed - speed ) : Utils::sign( wp.speed - speed ) * max_accel * dt;
       speed += d_speed;
       //speed = wp.speed;
       //vz
       /*
       double max_vz = 2;
       double vz_target = max_vz > std::abs( k * ( wp.alt - alt )  ) ? k * ( wp.alt - alt ) : Utils::sign( wp.alt - alt ) * max_vz;
       double max_az = 5;
       k = 10;
       double d_vz = max_az * dt > std::abs( k * ( vz_target - vz ) ) ? k * ( vz_target - vz ) : Utils::sign( vz_target - vz ) * max_az * dt;
       vz += d_vz;
       */
       vz = k * ( wp.alt - alt ) / 1;
       //x, y
       x += speed * cos( yaw ) * dt;
       y += speed * sin( yaw ) * dt;
       //alt
       alt += vz * dt;

       //t
       t += dt;

       //lat lon
       GetGCS();

       return *this;
   }

   bool StateSim::SeeArrive( const UserStructs::WaypointSim wp )
   {
       return std::abs( x - wp.x ) <= wp.x_err
           && std::abs( y - wp.y ) <= wp.y_err;
           //&& std::abs( alt - wp.alt ) <= wp.alt_err;
   }

   bool StateSim::CheckSpace( UserTypes::SpaceLimit space_limit )
   {
       return space_limit.TellIn( x, y, alt );
   }

   bool StateSim::CheckObss( std::vector< UserStructs::obstacle > obss,
                        const double h_thres,
                        const double v_thres )
   {
       for( int i = 0; i != obss.size(); ++i )
       {
           double dt = t - obss[i].t;
           UserStructs::obstacle obs = obss[i].extropolate( dt );

           double h_dis = std::sqrt( std::pow( obs.x - x, 2 )
                                   + std::pow( obs.y - y, 2 )
                                     );

           if( h_dis < h_thres
             && std::abs( alt - obs.z ) < v_thres ){
               return true;
           }
       }
       return false;
   }

   StateSim StateSim::SmallChange( double dt )
   {
       StateSim st;
       st.x = x + speed * cos( yaw ) * dt;
       st.y = y + speed * sin( yaw ) * dt;
       st.alt = alt + vz * dt;
       Utils::FromUTM( st.x, st.y, st.lon, st.lat );
       st.speed = speed;
       st.vz = vz;
       st.yaw = yaw;
       st.t = t + dt;

       return st;
   }

}

