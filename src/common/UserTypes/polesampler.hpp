#ifndef POLESAMPLER_HPP
#define POLESAMPLER_HPP

#include "planner/UserTypes/statesim.hpp"
#include "planner/UserStructs/waypointsim.h"

namespace UserTypes{
   class PoleSampler{
   public:
       PoleSampler();
       ~PoleSampler(){}

       void SetParams( const UserTypes::StateSim st_current,
                       const UserStructs::WaypointSim wp,
                       const double sig_ga);

       void GetSample( double &x_a,
                       double &y_a,
                       double &z_a );

   private:
       double x0;
       double y0;
       double z0;
       double ga0;
       double sigma_ga;
       double r0;
       double sigma_r;
       double theta0;
   };
}

#endif // POLESAMPLER_HPP

