#include "pathgenerator.hpp"
#include "common/Utils/yclogger.h"

namespace{
Utils::LoggerPtr s_logger(Utils::getLogger("ardrone_gps.PathGenerator.YcLogger"));
}

namespace UserTypes {

PathGenerator::PathGenerator():
      if_path_log( false ),
      if_in_ros( false ),
      wp_speed( 1.0 ),
      x_err( 2.0 ),
      y_err( 2.0 ),
      alt_err( 0.2 ),
      dt( 0.1 ),
      t_limit( 0.1 )
   {

   }

   /*1. set current st, set goal
    *2. set sample parameters
    *3. set obstacles, set spacelimit
    */
   int PathGenerator::AddPaths()
   {
      if( !if_in_ros ){
          ros::Time::init();
      }

      bool if_limit_reach = false;
      ros::Time t_start = ros::Time::now();
      double sec_count = ros::Time::now().toSec() - t_start.toSec();
      //clear previous path
      wp_lengths.clear();
      navigator.SetIfNavRec( true );

      sampler.SetParams( st_start,
                         wp_goal,
                         0.1 * M_PI);

      YCLOG( s_logger, LL_DEBUG, "path add begins" );

      while( 1 )
      {
         UserStructs::WaypointSim wp_inter = SamplePt();
         //YCLOG( s_logger, LL_DEBUG, "one run" );
         if( !navigator.PropWpCheck( st_start,
                                     wp_inter,
                                     dt,
                                     obstacles,
                                     space_limit )
           )
         {
             std::vector< UserTypes::StateSim > states_rec;
             navigator.GetNavRec( states_rec );
             for( int i = states_rec.size() - 1;
                  i >= 0;
                  --i )
             {
                 UserTypes::StateSim st_temp = states_rec[i];
                 if( !navigator.PropWpCheck( st_temp,
                                            wp_goal,
                                            dt,
                                            obstacles,
                                            space_limit)
                         )
                 {
                     double lat_s, lon_s;
                     Utils::FromUTM( st_temp.x,
                                     st_temp.y,
                                     lon_s,
                                     lat_s );

                     double length =
                             std::sqrt( std::pow( st_start.x - st_temp.x, 2 ) + std::pow( st_start.y - st_temp.y, 2) );
                     length += std::sqrt( std::pow( st_temp.x - wp_goal.x, 2 ) + std::pow( st_temp.y - wp_goal.y, 2) );

                     wp_lengths.push_back( UserStructs::WaypointLen( UserStructs::Waypoint( lat_s, lon_s, wp_goal.alt, wp_speed ), length ) );

                     //timer
                     if( !if_limit_reach )
                     {
                         sec_count = ros::Time::now().toSec()
                                 - t_start.toSec();
                         if( sec_count >= t_limit )
                         {
                             if_limit_reach = true;
                             break;
                         }
                     }//time limit ends

                 }
                 else{
                     //YCLOG( s_logger, LL_DEBUG, "second section failed");
                     break;
                 }
             }
         }
         else{
             //YCLOG( s_logger, LL_DEBUG, "first section fails" );
         }

         //timer
         if( !if_limit_reach )
         {
             sec_count = ros::Time::now().toSec()
                     - t_start.toSec();
             /*
             YCLOG( s_logger,
                    LL_DEBUG,
                    "sec_count:" << sec_count );
                    */
             if( sec_count >= t_limit )
             {
                 if_limit_reach = true;
             }
         }//time limit ends

         if( if_limit_reach )
         {
             YCLOG(s_logger,LL_DEBUG,"stop add intermediate path"
                    <<" "<< "time used: " << sec_count
                    <<" "<< "num of wps: " << wp_lengths.size() );
             YCLOG(s_logger,LL_DEBUG,"path adding ends");
            break;
         }

      }//while ends

      return wp_lengths.size();
   }

   //that is a free function for comparison
   bool WpCompFunc(UserStructs::WaypointLen wpl1, UserStructs::WaypointLen wpl2)
   {
     return (wpl1.length < wpl2.length);
   }

   bool PathGenerator::PathCheckRepeat( UserTypes::StateSim st_current )
   {
      //followiing AddPaths
      if( wp_lengths.empty() ){
          return false;
      }
      ros::Time t1 = ros::Time::now();
      //sort according to path length
      std::sort( wp_lengths.begin(),
                 wp_lengths.end(),
                 WpCompFunc );
      bool if_time_up = false;
      std::vector< UserTypes::StateSim > states_rec;

      for( int i = 0;
           i != wp_lengths.size();
           ++i )
      {
          double lat_w = wp_lengths[i].wp.lat;
          double lon_w = wp_lengths[i].wp.lon;
          double x_w, y_w;
          Utils::ToUTM( lon_w, lat_w, x_w, y_w );
          UserStructs::WaypointSim wp_w( x_w,
                                         y_w,
                                         wp_goal.alt,
                                         x_err,
                                         y_err,
                                         alt_err,
                                         wp_speed );
          if( !navigator.PropWpCheck(st_current,
                                     wp_w,
                                     dt,
                                     obstacles,
                                     space_limit)
            )
          {
              navigator.GetNavRec( states_rec );
              UserTypes::StateSim st_temp = navigator.GetStNow();
              if(  !navigator.PropWpCheck(st_temp,
                                         wp_goal,
                                         dt,
                                         obstacles,
                                         space_limit)
                )
              {
                  std::vector< UserTypes::StateSim > states_temp;
                  navigator.GetNavRec( states_temp );
                  states_rec.insert( states_rec.end(),
                                     states_temp.begin(),
                                     states_temp.end() );
                  wp_inter = wp_lengths[i].wp;
                  YCLOG( s_logger, LL_DEBUG,
                         "the " << i << "th"
                         " " << "wp was selected");
                  if( if_path_log ){
                      for( int i = 0;
                           i != states_rec.size();
                           ++i )
                      {
                         path_log << std::setprecision(6)
                                  << std::fixed
                                  << states_rec[i].x << ' '
                                  << states_rec[i].y << ' '
                                  << states_rec[i].alt
                                  << '\n';
                      }
                  }

                  return false;
                  break;
              }
          }
      }
      return true;
   }

   UserStructs::WaypointSim PathGenerator::SamplePt()
   {
      double x_a, y_a, z_a;
      while( 1 )
      {
         //YCLOG( s_logger, LL_DEBUG, "try to sample" );
         sampler.GetSample( x_a, y_a, z_a );
         /*
         YCLOG( s_logger, LL_DEBUG, "sample:"
                 << x_a << " "
                 << y_a << " "
                 << z_a );
                 */
         if( space_limit.TellIn(x_a,y_a,z_a) )
         {
             break;
         }
      }
      return UserStructs::WaypointSim( x_a,
                                       y_a,
                                       z_a,
                                       x_err,
                                       y_err,
                                       alt_err,
                                       wp_speed );
   }

   void PathGenerator::SetPathLogName( const char* filename )
   {
       try
       {
           path_log.exceptions ( std::ofstream::failbit | std::ofstream::badbit );
           path_log.open(filename,std::ofstream::out
                         | std::ofstream::in
                         | std::ofstream::trunc);
       }
       catch (std::ofstream::failure& e) {
           std::cerr << "Exception opening/reading file"
                     << e.what()
                     << std::endl;
       }
   }


}
