#include "pathgenerator.hpp"
//types and structs
#include "common/UserTypes/spacelimit.h"
#include "planner/UserTypes/statesim.hpp"
#include "planner/UserStructs/waypointsim.h"
#include "common/UserTypes/polesampler.hpp"
//utils
#include "common/Utils/yclogger.h"
#include "common/Utils/findpath.h"
//std
#include <iostream>

int main( int argc, char** argv )
{
    Utils::LogConfigurator myconfigurator("log4cxx_TestPathgen.properties","log for TestPathgen");

    UserTypes::PathGenerator path_gen;

    //space limit
    UserTypes::SpaceLimit spacelimit( 2.5, 1 );
    spacelimit.LoadGeoFence( (Utils::FindPath()+ "records/geo_fence_narrow.txt").c_str() );
    path_gen.SetSpaceLimit( spacelimit );

    //time limit
    path_gen.SetTimeLimit( 0.1 );

    //if in_ros
    path_gen.SetIfInRos( false );

    //set h_thres v_thres
    path_gen.SetHVThres( 2.0, 1.0 );

    //init state
    //0 33.409974 -111.893975 416875.644012 3697095.333840 360.490000 0.230000 1.058653 -0.990527
    double x_st = 416875.644012;
    double y_st = 3697095.333840 - 5;
    double lat_st, lon_st;
    Utils::FromUTM( x_st, y_st, lon_st, lat_st );
    UserTypes::StateSim st_init(0,
                                x_st,
                                y_st,
                                2,
                                lat_st,
                                lon_st,
                                0.23,
                                1.058653,
                                -0.990527 + M_PI / 2);
    path_gen.SetCurrentSt( st_init );

    //goal
    double lat_g = 33.4102420;
    double lon_g = -111.8940072;
    double alt_g = 2;
    double x_g, y_g;
    Utils::ToUTM( lon_g, lat_g, x_g, y_g );
    x_g = st_init.x;
    double x_err = 2;
    double y_err = 2;
    double alt_err = 0.2;
    double wp_speed = 1.;
    UserStructs::WaypointSim wp_goal( x_g,
                                      y_g,
                                      alt_g,
                                      x_err,
                                      y_err,
                                      alt_err,
                                      wp_speed );
    path_gen.SetGoalWp( wp_goal );

    //obstacle
    double x_obs = ( x_st + x_g ) / 2;
    double y_obs = ( y_st + y_g ) / 2;
    double z_obs = 2.;
    double heading = atan2( y_st - y_g, x_st - x_g );
    double spd = 1.;
    double vzz = 0.;
    std::vector< UserStructs::obstacle > obss;

    obss.push_back( UserStructs::obstacle(0,
                                          x_obs,
                                          y_obs,
                                          z_obs,
                                          heading,
                                          spd,
                                          vzz) );

    /*
    UserTypes::NavigatorSim navigator;
    navigator.SetIfLogProg( true );
    navigator.SetProLogName("sim_traj.txt");
    bool result = navigator.PropWpCheck( st_init,
                                         wp_goal,
                                         0.1,
                                         obss,
                                         spacelimit );
                                         std::cout << "navi check result:" << result << '\n';
    navigator.PropagateWp( st_init, wp_goal, 0.1 );
    */

    path_gen.SetObss( obss );
    path_gen.SetPathLogName( "path_log.txt");
    path_gen.SetTimeLimit( 0.1 );
    path_gen.SetIfPathLog( true );

    std::cout << "path num:"
              << path_gen.AddPaths()
              << '\n';

    std::cout << "path check:"
              << path_gen.PathCheckRepeat( st_init.WpUpdate( wp_goal, 0.1 ) )
              << '\n';
}
