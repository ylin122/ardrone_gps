#include "navigatorsim.hpp"
#include <fstream>
#include <sstream>
#include <iostream>
#include <cmath>
#include "common/Utils/utmtransform.h"

int main( int argc, char** argv )
{
    UserTypes::NavigatorSim navigator;
    navigator.SetIfLogProg( true );
    navigator.SetProLogName("sim_traj17.txt");

    //get the initial state;

    std::string line;
    std::ifstream infile("/home/yucong/catkin_ws/src/ardrone_gps/records/traj_log17.txt");
    UserTypes::StateSim uav_st;
    while (std::getline(infile, line))
    {
        std::istringstream iss(line);

        int uav_state_idx;
        if ( ! ( iss >> uav_st.t
                 >> uav_st.lat
                 >> uav_st.lon
                 >> uav_st.x
                 >> uav_st.y
                 >> uav_st.alt
                 >> uav_st.speed
                 >> uav_st.vz
                 >> uav_st.yaw
                 >> uav_st.X_ref
                 >> uav_st.X_traj
                 >> uav_st.Y_ref
                 >> uav_st.Y_traj
                 >> uav_st.vx_traj
                 >> uav_st.vy_traj
                 >> uav_state_idx
                 )
             )
        {
            break;
        } // error

        if( uav_state_idx == 3 ){
            uav_st.alt = 1.54;
            uav_st.yaw += M_PI / 2;
            break;
        }
    }

    //waypoint to go
    //double lat_s = 33.4102375;//diagnose
    //double lon_s = -111.8937229;
    //double lat_s = 33.4102476; //north
    //double lon_s = -111.8939441;
    double lat_s = 33.410139;
    double lon_s = -111.893931;
    double x_s, y_s;
    Utils::ToUTM( lon_s, lat_s, x_s, y_s );
    double alt_s = 1.5;
    double x_err = 2.0;
    double y_err = 2.0;
    double alt_err = 0.2;
    double speed_d = 2.;

    UserStructs::WaypointSim wp( x_s, y_s, alt_s, x_err, y_err, alt_err, speed_d );

    //set init state from the north
    /*
    double lat_start = 33.4102476;
    double lon_start = -111.8939441;
    double x_start, y_start;
    Utils::ToUTM( lon_start,
                  lat_start,
                  x_start,
                  y_start );

    double lat_end = 33.409920;
    double lon_end = -111.893968;
    double x_end, y_end;
    Utils::ToUTM( lon_end,
                  lat_end,
                  x_end,
                  y_end );

    double heading = atan2( y_end - y_start,
                            x_end - x_start );
    double speed = 1.0;
    double alt = 1.5;

    UserTypes::StateSim uav_st( 0,
                                x_start,
                                y_start,
                                alt,
                                lat_start,
                                lon_start,
                                speed,
                                0,
                                heading
                                );

    UserStructs::WaypointSim wp( x_end,
                                 y_end,
                                 alt,
                                 2,
                                 2,
                                 0.2,
                                 speed );
    */

    //propagate the trajectory
    /*
    std::cout << "uav_st alt:" << uav_st.alt
              << ' '
              << "uav_st yaw:" << uav_st.yaw * 180 / M_PI
              << '\n';
              */
    double dt = 0.1;
    navigator.PropagateWp( uav_st, wp, dt );

}
