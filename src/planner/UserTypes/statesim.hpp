#pragma once
#include "planner/UserStructs/waypointsim.h"
#include "common/UserTypes/spacelimit.h"
#include "planner/UserStructs/obstacle.h"
#include <vector>

namespace UserTypes{
   class StateSim{
   public:
       StateSim();
       StateSim( double _t,
                 double _x,
                 double _y,
                 double _alt,
                 double _lat,
                 double _lon,
                 double _speed,
                 double _vz,
                 double _yaw,
                 double _X_traj,
                 double _X_ref,
                 double _Y_traj,
                 double _Y_ref,
                 double _vx_traj,
                 double _vy_traj );

       StateSim( double _t,
                 double _x,
                 double _y,
                 double _alt,
                 double _lat,
                 double _lon,
                 double _speed,
                 double _vz,
                 double _yaw );

       ~StateSim(){ };

       double t;
       double x;
       double y;
       double alt;
       double lat;
       double lon;
       double speed;
       double yaw;
       double vz;
       //extra
       float X_traj;
       float X_ref;
       float Y_traj;
       float Y_ref;
       float vx_traj;
       float vy_traj;
       //fuctions
       void GetUTM();
       void GetGCS();
       void CheckConvert();

       StateSim WpUpdate( const UserStructs::WaypointSim wp, double dt );

       bool SeeArrive( const UserStructs::WaypointSim wp );

       bool CheckSpace( UserTypes::SpaceLimit space_limit );

       bool CheckObss( std::vector< UserStructs::obstacle > obss,
                      const double h_thres,
                      const double v_thres );

       StateSim SmallChange( double dt );
     private:
   };
}
