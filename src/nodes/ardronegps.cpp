#include "ardronegps.hpp"
#include "common/Utils/yclogger.h"
#include "common/Utils/gettimenow.h"
#include "common/Utils/utmtransform.h"
#include "common/UserStructs/constants.h"
#include "common/Utils/waypointandsim.h"
#include "common/Utils/findpath.h"

#include <cmath>
#include <iostream>
#include <iomanip>

#include <std_msgs/Empty.h>
#include <std_srvs/Empty.h>
#include <geographic_msgs/WayPoint.h>
#include <ardrone_autonomy/RecordEnable.h>
#include <ardrone_autonomy/SetGPSTarget.h>
#include <ardrone_gps/ArdroneState.h>

namespace{
Utils::LoggerPtr s_logger(Utils::getLogger("ardrone_gps.ArdroneGPS.YcLogger"));
}

namespace Nodes{

ArdroneGPS::ArdroneGPS():zm_pre( 0.0 ),
                         if_joy( false ),
                         uav_state_pre( 0 ),
                         lat_s( 0. ),
                         lon_s( 0. ),
                         alt_s( 0. ),
                         if_real_obs( false ),
                         if_take_off( false ),
                         if_stable( false ),
                         //monitor( UserTypes::OthersMonitor(0,0) ),
                         wp_idx( -1 ),
                         dt( 0.1 ),
                         wp_sent_done( false ),
                         planning_num( 0 )
{
    //publishers
    pub_vel = nh.advertise<geometry_msgs::Twist>("cmd_vel", 1);
    takeoff_pub = nh.advertise<std_msgs::Empty>("ardrone/takeoff",1);
    land_pub = nh.advertise<std_msgs::Empty>("ardrone/land",1);
    emergency_pub = nh.advertise<std_msgs::Empty>("ardrone/reset",1);
    pub_if_take_off = nh.advertise<std_msgs::Bool>("if_take_off",1);
    pub_if_stable = nh.advertise<std_msgs::Bool>("if_stable",1);
    pub_state = nh.advertise< ardrone_gps::ArdroneState >( "quad_state", 1 );
    //subscribers
    joy_sub = nh.subscribe(nh.resolveName("joy"), 1, &ArdroneGPS::joyCb, this);
    nav_sub = nh.subscribe("ardrone/navdata", 1, &ArdroneGPS::navdataCb, this);
    gps_sub = nh.subscribe("ardrone/navdata_gps", 1, &ArdroneGPS::navdataGPSCb, this);
    imu_sub = nh.subscribe("ardrone/imu", 1, &ArdroneGPS::imuCb, this);

    //service
    client_setgpstarget = nh.serviceClient< ardrone_autonomy::SetGPSTarget >("ardrone/setgpstarget");
    client_setautoflight = nh.serviceClient< ardrone_autonomy::RecordEnable >("ardrone/setautoflight");
    flattrim_srv= nh.serviceClient<std_srvs::Empty>("ardrone/flattrim",1);
    tkm1 = ros::Time::now();
    lastR1Pressed= false;
    lastL1Pressed= false;
}

void ArdroneGPS::SubWork()
{
    //try to get obstacles
    UpdateObss( obstacles );

    //send the goal waypoint
    if( !wp_sent_done )
    {
        if( !if_real_obs )
        {
            if( SendWpTest( goal.lat,
                            goal.lon,
                            goal.alt,
                            goal.speed )
                    )
            {
                wp_sent_done = true;
            }
        }
        else
        {
            double lat_w, lon_w;
            if( !obstacles.empty() )
            {
                Utils::FromUTM( obstacles[0].x,
                        obstacles[0].y,
                        lat_w,
                        lon_w
                        );
            }
            else
            {
                lat_w = goal.lat;
                lon_w = goal.lon;
            }
            //send the waypoint
            if( SendWpTest( lat_w, lon_w, goal.alt, goal.speed ) )
            {
                YCLOG( s_logger, LL_DEBUG,
                       "host sent to waypoint:"
                       << lat_w << ' '
                       << lon_w << ' '
                       << goal.alt );
            }
        }
    }

    switch( situ )
    {
       case NORMAL:
       {
          //predict for collisions
          if( uav_state_idx != 2 )
          {
            if( PredictColli() == 1
              && planning_num < 1
                    )
            {
                situ = PATH_GEN;
            }
          }
          break;
       }
       case PATH_GEN:
       {

          if( wp_sent_done )
          {
             YCLOG( s_logger, LL_DEBUG, "planning" );
             path_gen.SetIfInRos( true );
             path_gen.SetCurrentSt( uav_st.SmallChange( t_limit) );
             path_gen.SetGoalWp( Utils::WpToSim( waypoints[0]) );
             path_gen.SetObss( obstacles );
             int n_paths = path_gen.AddPaths();
             YCLOG( s_logger, LL_DEBUG, "path number:" << n_paths );
             if( n_paths > 0 ){
                 situ = PATH_CHECK;
             }
             else{
                 YCLOG( s_logger,
                        LL_DEBUG,
                        "no path, retry" );
             }
          }
          else{
              YCLOG( s_logger,
                     LL_DEBUG,
                     "want to plan, but goal waypoint not sent yet" );
          }
          break;
       }
       case PATH_CHECK:
       {
          if( !path_gen.PathCheckRepeat( uav_st ) )
          {
              YCLOG( s_logger,
                     LL_DEBUG,
                     "check ok, yes waypoint");
              ++ planning_num;
              situ = PATH_READY;
          }
          else{
              YCLOG( s_logger,
                     LL_DEBUG,
                     "no waypoint, retry");
              situ = PATH_GEN;
          }
          break;
       }
       case PATH_READY:
       {
          UserStructs::Waypoint wp_inter = path_gen.GetInterWp();
          YCLOG( s_logger,
                 LL_DEBUG, "avoidance wp_inter:"
                 << wp_inter.lat << ' '
                 << wp_inter.lon << ' '
                 << wp_inter.alt << ' '
                 << wp_inter.speed );

          avoid_wp_log << std::setprecision(10)
                       << std::fixed
                       << wp_inter.lat << ' '
                       << wp_inter.lon << ' '
                       << wp_inter.alt << ' '
                       << wp_inter.speed << '\n';

          if( InsertWpTest( wp_inter.lat,
                        wp_inter.lon,
                        wp_inter.alt,
                        wp_inter.speed )
            )
          {
             situ = NORMAL;
          }
          break;
       }
       default:
       {
          break;
       }
    }//switch( situ ) ends
}

void ArdroneGPS::UpdateObss( std::vector<UserStructs::obstacle> &obss )
{
    obsupdater -> UpdateObs( obss );
    for( int i = 0; i != obss.size(); ++i )
    {
         double dis_h = std::sqrt( pow( obss[i].x - uav_st.x, 2) + pow( obss[i].y - uav_st.y, 2 ) );
         double dis_v = std::abs( uav_st.alt - obss[i].z );
         std::ostringstream oss;
         oss << std::setprecision(6) << std::fixed
             << i << " "
             << "t:" << obss[i].t << ' '
             << "x:" << obss[i].x << ' '
             << "y:" << obss[i].y << ' '
             << "z:" << obss[i].z << ' '
             << "heading:" << obss[i].heading * 180. / M_PI << " "
             << "speed:" << obss[i].speed << ' '
             << "dis_h:" << dis_h << ' '
             << "dis_v:" << dis_v << '\n';
         //YCLOG( s_logger, LL_DEBUG, oss.str() );

         obdis_log << oss.str();
    }
}

void ArdroneGPS::GetVirtualObs()
{
    UserTypes::NavigatorSim navigator;
    navigator.SetIfLogProg( true );
    navigator.SetProLogName( (Utils::FindPath() + "records/obs_traj.txt").c_str() );

    double lat_start = goal.lat;
    double lon_start = goal.lon;
    double x_start, y_start;
    Utils::ToUTM( lon_start,
                  lat_start,
                  x_start,
                  y_start );

    double lat_end = uav_st.lat;
    double lon_end = uav_st.lon;
    double x_end, y_end;
    Utils::ToUTM( lon_end,
                  lat_end,
                  x_end,
                  y_end );

    double heading = atan2( y_end - y_start,
                            x_end - x_start );
    double speed = goal.speed;
    double alt = goal.alt;

    UserTypes::StateSim uav_st( 0,
                                x_start,
                                y_start,
                                alt,
                                lat_start,
                                lon_start,
                                speed,
                                0,
                                heading
                                );

    UserStructs::WaypointSim wp( x_end,
                                 y_end,
                                 alt,
                                 2,
                                 2,
                                 0.2,
                                 speed );

    //propagate the trajectory
    /*
    std::cout << "uav_st alt:" << uav_st.alt
              << ' '
              << "uav_st yaw:" << uav_st.yaw * 180 / M_PI
              << '\n';
              */
    double dt = 0.1;
    navigator.PropagateWp( uav_st, wp, dt );
}

void ArdroneGPS::working4()
{
    ros::Rate r( 10 );
    while( ros::ok() )
    {
        ros::spinOnce();

        PubIfTakeOff();
        PubIfStable();

        monitor -> ifOthersStable();

        UpdateObss( obstacles );
        GetCurrentSt();
        PubState();

        r.sleep();
    }
}

void ArdroneGPS::working3( double _speed, double _hgt )
{
    bool if_start = false;
    bool taken_off = false;
    //
    ros::Rate r(10);
    while( ros::ok() )
    {
        ros::spinOnce();

        PubIfTakeOff();
        PubIfStable();

        GetCurrentSt();
        PubState();

        if( !taken_off && uav_state_idx == 2 ){
            sendTakeoff();
            taken_off = true;
        }

        if( uav_state_idx != 2 ){
            if_take_off = true;
        }

        if( !if_start )
        {
            if( uav_state_pre!= 4
             && uav_state_idx == 4 )
            {
                if_stable = true;
            }
            if( monitor -> ifOthersStable()
             && if_stable )
            {
                if_start= true;
                double lat_w, lon_w;
                if( !obstacles.empty() )
                {
                    Utils::FromUTM( obstacles[0].x,
                            obstacles[0].y,
                            lat_w,
                            lon_w
                            );
                }
                else
                {
                    lat_w = 33.409953;
                    lon_w = -111.893959;
                }
                //send the waypoint
                if( SendWpTest( lat_w, lon_w, goal.alt, goal.speed ) )
                {
                    YCLOG( s_logger, LL_DEBUG,
                           "host sent to waypoint:"
                           << lat_w << ' '
                           << lon_w << ' '
                           << goal.alt );
                }
            }
        }

        uav_state_pre = uav_state_idx;
        CheckWpNav();
        r.sleep();

    }
}

void ArdroneGPS::working2()
{
    bool if_start = false;
    bool taken_off = false;
    //
    ros::Rate r(10);
    while( ros::ok() )
    {
        YCLOG( s_logger, LL_DEBUG, "each loop" );
        SetToDefault();
        ros::spinOnce();

        PubIfTakeOff();
        PubIfStable();

        GetCurrentSt();
        PubState();

        if( !taken_off && uav_state_idx == 2 ){
            sendTakeoff();
            taken_off = true;
        }

        if( uav_state_idx != 2 ){
            if_take_off = true;
        }

        if( !if_start )
        {
            if( uav_state_pre!= 4
             && uav_state_idx == 4 )
            {
                if_stable = true;
                YCLOG( s_logger,
                       LL_DEBUG,
                       "this stable" );
            }
            if( monitor -> ifOthersStable()
             && if_stable )
            {
                if_start= true;
                situ = NORMAL;
                if( !if_real_obs )
                {
                   GetVirtualObs();
                   std::vector< std::string > filenames;
                   filenames.push_back( Utils::FindPath()+"records/obs_traj.txt");
                   obsupdater -> ReadObss( filenames );
                }
            }
        }
        else
        {
            SubWork();
        }

        uav_state_pre = uav_state_idx;
        CheckWpNav();
        r.sleep();
    }
}

void ArdroneGPS::working()
{
    bool taken_off = false;
    bool wp_sent_done = false;
    bool wp_sending_start = false;
    bool wp_inserted = false;
    bool if_start = false;
    ros::Time t_wp_sent = ros::Time::now();
    t_transit = ros::Time::now();

    ros::Rate r(10);
    while( ros::ok() )
    {
        nav_gps.data_available = -1;
        ros::spinOnce();

        PubIfTakeOff();
        PubIfStable();

        GetCurrentSt();
        PubState();

        //try to get obstacles
        UpdateObss( obstacles );

        if( !taken_off && uav_state_idx == 2 ){
            sendTakeoff();
            taken_off = true;
        }

        //send a GPS waypoint
        if( uav_state_pre != 4
           && uav_state_idx == 4 )
        {
           wp_sending_start = true;
           if_stable = true;
        }

        if( monitor -> ifOthersStable()
         && if_stable )
        {
            if_start = true;
        }

        if( if_start )
        {
            if( wp_sending_start && !wp_sent_done )
            {
                lat_s = 33.4102420, lon_s = -111.8940072;
                //lat_s = 33.4101703, lon_s = -111.8939428;
                alt_s = 1.5;
                double spd_d = 1.0;
                if( SendWpTest( lat_s, lon_s, alt_s, spd_d ) )
                {
                    wp_sending_start = false;
                    wp_sent_done = true;
                    t_wp_sent = ros::Time::now();
                }
            }

            if( ros::Time::now().toSec() - t_wp_sent.toSec() > 3.0
                    && wp_sent_done
                    && !wp_inserted )
            {
                //lat_s = 33.4101121, lon_s = -111.8938771;
                //lat_s = 33.410012, lon_s = -111.893830; //between 0 and 1 bar
                //lat_s = 33.4100763, lon_s = -111.8938825;
                //lat_s = 33.410101, lon_s = -111.893845; //between 1 and 2 bar
                lat_s = 33.410105, lon_s = -111.89388; //closer to the left one
                //lat_s = 33.409996, lon_s = -111.894010; //left between 0 and 1 bar
                //lat-s = 33.410080, lon_s = -111.894011; //left between 1 and 2 bar

                alt_s = 1.5;
                double spd_d = 1.0;
                if( InsertWpTest( lat_s, lon_s, alt_s, spd_d ) )
                {
                    wp_inserted = true;
                }

            }

            CheckWpNav();
        }
        uav_state_pre = uav_state_idx;
        r.sleep();
    }   //while ends

}

void ArdroneGPS::imuCb(const sensor_msgs::ImuPtr imu_msg)
{
    double qx = imu_msg -> orientation.x;
    double qy = imu_msg -> orientation.y;
    double qz = imu_msg -> orientation.z;
    double qw = imu_msg -> orientation.w;

    tf::Matrix3x3 m( tf::Quaternion(qx,qy,qz,qw) );
    double roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);

    YCLOG(s_logger,LL_DEBUG,"RPY:"
           << " " << roll * UasCode::RAD2DEG
           << " " << pitch * UasCode::RAD2DEG
           << " " << yaw * UasCode::RAD2DEG);
}

void ArdroneGPS::navdataGPSCb( const ardrone_autonomy::navdata_gpsConstPtr navdata_gpsPtr )
{
    YCLOG( s_logger, LL_DEBUG, "gps data available:"
           << (int)navdata_gpsPtr -> data_available );

    nav_gps.data_available = navdata_gpsPtr -> data_available;
    if( (int) navdata_gpsPtr -> data_available > 0 )
    {
        nav_gps.latitude = navdata_gpsPtr -> latitude;
        nav_gps.longitude = navdata_gpsPtr -> longitude;
        nav_gps.elevation = navdata_gpsPtr -> elevation;

        nav_gps.lat0 = navdata_gpsPtr -> lat0;
        nav_gps.long0 = navdata_gpsPtr -> long0;
        nav_gps.lat_fused = navdata_gpsPtr -> lat_fused;
        nav_gps.long_fused = navdata_gpsPtr -> long_fused;
        nav_gps.gps_state = navdata_gpsPtr -> gps_state;

        nav_gps.X_traj = navdata_gpsPtr -> X_traj;
        nav_gps.X_ref = navdata_gpsPtr -> X_ref;
        nav_gps.Y_traj = navdata_gpsPtr -> Y_traj;
        nav_gps.Y_ref = navdata_gpsPtr -> Y_ref;

        nav_gps.speed = navdata_gpsPtr -> speed;
        nav_gps.is_gps_plugged = navdata_gpsPtr -> is_gps_plugged;
        nav_gps.ehpe = navdata_gpsPtr -> ehpe;
        nav_gps.ehve = navdata_gpsPtr -> ehve;
        nav_gps.vx_traj = navdata_gpsPtr -> vx_traj;
        nav_gps.vy_traj = navdata_gpsPtr -> vy_traj;

        std::ostringstream oss;
        oss << "gps data:" << '\n'
            << std::setprecision(5)<< std::fixed
            << "latitude:" << nav_gps.latitude << ' '
            << "longitude:" << nav_gps.longitude << ' '
            << "elevation:" << nav_gps.elevation << '\n'
            << "lat0:" << nav_gps.lat0 << ' '
            << "long0:" << nav_gps.long0 << '\n'
            << "lat_fused:" << nav_gps.lat_fused << ' '
            << "long_fused:" << nav_gps.long_fused << '\n'
            << "gps_state:" << nav_gps.gps_state << '\n'
            << "X_traj:" << nav_gps.X_traj << ' '
            << "X_ref:" << nav_gps.X_ref << ' '
            << "Y_traj:" << nav_gps.Y_traj << ' '
            << "Y_ref:" << nav_gps.Y_ref << '\n'
            << "speed:" << nav_gps.speed << '\n '
            << "ehpe:" << nav_gps.ehpe << ' '
            << "ehve:" << nav_gps.ehve << '\n'
            << "vx_traj:" << nav_gps.vx_traj << ' '
            << "vy_traj:" << nav_gps.vy_traj << '\n'
            << "number of sat:" << (int)nav_gps.nbsat << ' '
            << "is_gps_plugged:" << nav_gps.is_gps_plugged << '\n'
            << "degree:" << navdata_gpsPtr -> degree << ' '
            << "degree magnetic:" << navdata_gpsPtr -> degree_magnetic
            << std::endl;
        YCLOG( s_logger, LL_DEBUG, oss.str() );
    }
    else{
        YCLOG( s_logger, LL_DEBUG, "gps data unavailable");
    }
}

void ArdroneGPS::navdataCb(const ardrone_autonomy::NavdataConstPtr navdataPtr)
{
    tk = ros::Time::now();
    elapsed_time = tk - tkm1;
    double elapsed_time_dbl = elapsed_time.sec + (double)elapsed_time.nsec / 1E9;

    yaw_est = (double)navdataPtr -> rotZ * M_PI / 180;
    if(yaw_est> M_PI) {
        yaw_est-= 2*M_PI;
    }
    else if(yaw_est< -M_PI) {
        yaw_est+= 2*M_PI;
    }
    else{
    }

    zm = navdataPtr->altd/1000.;
    vz =( zm - zm_pre ) / elapsed_time_dbl;

    uav_state_idx = navdataPtr -> state;

    tkm1 = tk;
    zm_pre = zm;

    double battery_percent = navdataPtr -> batteryPercent;
    YCLOG( s_logger, LL_DEBUG, "battery percent:" << battery_percent );
    if( battery_percent < 0.2
     && uav_state_idx != 2
     && uav_state_idx != 8)
    {
        YCLOG( s_logger, LL_DEBUG, "battery less than 20%, land!!!" );
        sendLand();
    }
    PrintState();
}

void ArdroneGPS::joyCb( const sensor_msgs::JoyConstPtr joy_msg )
{
    if(joy_msg->axes.size() < 4) {
        ROS_WARN_ONCE("Error: Non-compatible Joystick!");
        return;
    }
    // Avoid crashes if non-ps3 joystick is being used
    short unsigned int L1 = 4;
    short unsigned int R1 = 5;
    int YAW= 0;
    int GAZ= 1;
    int ROLL= 3;
    int PITCH= 4;

    if( joy_msg->buttons.at(2) ){
        if_joy = false;
    }

    bool justStartedControlling = false;
    if(joy_msg->axes[YAW]>0.1||joy_msg->axes[YAW]<-0.1||
            joy_msg->axes[GAZ]>0.1||joy_msg->axes[GAZ]< -0.1||
            joy_msg->axes[ROLL]>0.1||joy_msg->axes[ROLL]<-0.1||
            joy_msg->axes[PITCH]>0.1||joy_msg->axes[PITCH]<-0.1||
            joy_msg->buttons.at(R1) //|| joy_msg->buttons.at(L1)
            )
    {
        if_joy = true;
        justStartedControlling = true;
        YCLOG( s_logger, LL_DEBUG, "start joystick control");
    }

    // are we actually controlling with the Joystick?
    if( justStartedControlling || if_joy )
    {
        //set to auto
        ardrone_autonomy::RecordEnable set_auto_srv;
        set_auto_srv.request.enable = false;
        client_setautoflight.call( set_auto_srv );

        ControlCommand c;
        c.yaw = joy_msg->axes[YAW];
        c.gaz = joy_msg->axes[GAZ];
        c.roll = joy_msg->axes[ROLL];
        c.pitch = -joy_msg->axes[PITCH];

        SendControlToDrone(c);

        if( joy_msg->buttons.at(L1) )
        {
            if( uav_state_idx == 2 ){
                YCLOG( s_logger, LL_DEBUG, "trying to take off" );
                sendTakeoff();
            }
            if( uav_state_idx == 3 || uav_state_idx == 7 ){
                SendControlToDrone( ControlCommand(0,0,0,0) );
                sendLand();
            }
            if( uav_state_idx == 4 )
                sendLand();
        }//if joy_msg ends

        if(!lastR1Pressed && joy_msg->buttons.at(R1))
            sendEmergencyStop();
    }
    lastL1Pressed =joy_msg->buttons.at(L1);
    lastR1Pressed = joy_msg->buttons.at(R1);
    if( if_joy ) {
        YCLOG( s_logger, LL_DEBUG, "currently using joystick" );
    }
}

void ArdroneGPS::SendControlToDrone(ControlCommand cmd)
{
   // TODO: check converstion (!)
   geometry_msgs::Twist cmdT;
   cmdT.angular.z = cmd.yaw;
   cmdT.linear.z = cmd.gaz;
   cmdT.linear.x = -cmd.pitch;
   cmdT.linear.y = cmd.roll;
   pub_vel.publish(cmdT);
}

void ArdroneGPS::sendLand()
{
   land_pub.publish(std_msgs::Empty());
   YCLOG( s_logger, LL_DEBUG, "sent to land");
}

void ArdroneGPS::sendFlatTrim()
{
   std_srvs::Empty flattrim_srv_srvs;
   flattrim_srv.call( flattrim_srv_srvs );
}

void ArdroneGPS::sendTakeoff()
{
   takeoff_pub.publish(std_msgs::Empty());
   YCLOG( s_logger, LL_DEBUG, "sent to take off" );
}

void ArdroneGPS::sendStop()
{
   SendControlToDrone( ControlCommand(0,0,0,0) );
   YCLOG( s_logger, LL_DEBUG, "sent to stop" );
}

void ArdroneGPS::sendEmergencyStop()
{//send to emergency stop
   emergency_pub.publish(std_msgs::Empty() );
   YCLOG( s_logger, LL_DEBUG, "sent to emergency stop" );
}

void ArdroneGPS::PubIfTakeOff()
{
   std_msgs::Bool msg;
   msg.data = if_take_off;
   pub_if_take_off.publish(msg);
}

void ArdroneGPS::PubIfStable()
{
   std_msgs::Bool msg;
   msg.data = if_stable;
   YCLOG( s_logger,
          LL_DEBUG,
          "if_stable pub:" << if_stable );
   pub_if_stable.publish(msg);
}

void ArdroneGPS::PubState()
{
   ardrone_gps::ArdroneState state_msg;
   state_msg.t = Utils::GetTimeNow();
   state_msg.x = uav_st.x;
   state_msg.y = uav_st.y;
   state_msg.z = uav_st.alt;
   state_msg.yaw = uav_st.yaw;
   state_msg.speed = uav_st.speed;
   state_msg.vz = uav_st.vz;

   pub_state.publish( state_msg );
}

void ArdroneGPS::SetToDefault()
{
   nav_gps.data_available = -1;
}

void ArdroneGPS::PrintState()
{
    switch( uav_state_idx ){
       case 0:
        YCLOG( s_logger, LL_DEBUG, "uav_state: UNKNOWN");
        break;
       case 1:
        YCLOG( s_logger, LL_DEBUG, "uav_state: Inited");
        break;
       case 2:
        YCLOG( s_logger, LL_DEBUG, "uav_state: Landed");
        break;
       case 3:
        YCLOG( s_logger, LL_DEBUG, "uav_state: Flying");
        break;
       case 4:
        YCLOG( s_logger, LL_DEBUG, "uav_state: Hovering");
        break;
       case 5:
        YCLOG( s_logger, LL_DEBUG, "uav_state: Test");
        break;
       case 6:
        YCLOG( s_logger, LL_DEBUG, "uav_state: Taking off");
        break;
       case 7:
        YCLOG( s_logger, LL_DEBUG, "uav_state: Flying");
        break;
       case 8:
        YCLOG( s_logger, LL_DEBUG, "uav_state: Landing");
        break;
       case 9:
        YCLOG( s_logger, LL_DEBUG, "uav_state: Looping");
        break;
       default:
        break;
    }
}

void ArdroneGPS::GetCurrentSt()
{
    //YCLOG( s_logger, LL_DEBUG, "gps data available:" << (int)nav_gps.data_available );
    if( (int)nav_gps.data_available > 0 )
    {
        uav_st.t = Utils::GetTimeNow();
        uav_st.lat = nav_gps.latitude;
        uav_st.lon = nav_gps.longitude;
        //uav_st.alt = nav_gps.elevation;
        uav_st.alt = zm;
        uav_st.GetUTM();
        uav_st.speed = nav_gps.speed;
        uav_st.yaw = yaw_est + M_PI / 2;

        uav_st.X_ref = nav_gps.X_ref;
        uav_st.X_traj = nav_gps.X_traj;
        uav_st.Y_ref = nav_gps.Y_ref;
        uav_st.Y_traj = nav_gps.Y_traj;
        uav_st.vx_traj = nav_gps.vx_traj;
        uav_st.vy_traj = nav_gps.vy_traj;

        YCLOG( s_logger, LL_DEBUG, "yaw_est:"
               << yaw_est * 180./ M_PI );
        YCLOG( s_logger, LL_DEBUG, "yaw_uav:"
               << uav_st.yaw * 180./ M_PI );
        uav_st.vz = vz;

        if( uav_state_idx == 3 || uav_state_idx == 7 )
        {
            traj_log << std::setprecision(6) << std::fixed
                     << uav_st.t << ' '
                     << uav_st.lat << ' '
                     << uav_st.lon << ' '
                     << uav_st.x << ' '
                     << uav_st.y << ' '
                     << uav_st.alt << ' '
                     << uav_st.speed << ' '
                     << uav_st.vz << ' '
                     << uav_st.yaw << ' '
                     << uav_st.X_ref << ' '
                     << uav_st.X_traj << ' '
                     << uav_st.Y_ref << ' '
                     << uav_st.Y_traj << ' '
                     << uav_st.vx_traj << ' '
                     << uav_st.vy_traj << ' '
                     << uav_state_idx << '\n';
        }
    }
}

bool ArdroneGPS::CheckArriveWp()
{
    if( lat_s != 0.
      && lon_s != 0.
      && alt_s != 0.)
    {
        double x_s, y_s;
        Utils::ToUTM( lon_s, lat_s, x_s, y_s );
        double wp_dis = std::sqrt( std::pow( x_s - uav_st.x, 2 )
                              + std::pow( y_s - uav_st.y, 2 ) );

        double take_dis = std::sqrt( std::pow( x_take - uav_st.x, 2 )
                              + std::pow( y_take - uav_st.y, 2 ) );
        YCLOG( s_logger, LL_DEBUG,
               "wp_dis:" << wp_dis << ' '
               << "take_dis:" << take_dis);

        if( nav_gps.X_ref != 0 &&
            nav_gps.X_ref == nav_gps.X_traj &&
            nav_gps.Y_ref != 0 &&
            nav_gps.Y_ref == nav_gps.Y_traj &&
            take_dis > 2.0 &&
            wp_sent_done
           )
        //if( wp_dis < 2.0 )
        {
            if( uav_state_idx != 2 && uav_state_idx != 8 )
            {
                sendStop();
                sendLand();
                YCLOG( s_logger, LL_DEBUG, "arrive, land");
                YCLOG( s_logger, LL_DEBUG, "end compare:" << '\n'
                       << std::abs( x_s - uav_st.x ) << ' '
                       << nav_gps.ehpe << '\n'
                       << std::abs( y_s - uav_st.y ) << ' '
                       << nav_gps.ehve << '\n' );
            }
        }
    }
}

bool ArdroneGPS::SendWaypoint(const double _lat,
                              const double _lon,
                              const double _alt,
                              const double _vel)
{
    if( (int)nav_gps.data_available > 0 )
    {
        ardrone_autonomy::SetGPSTarget set_gps_srv;
        set_gps_srv.request.target.position.latitude = _lat;
        set_gps_srv.request.target.position.longitude = _lon;
        set_gps_srv.request.target.position.altitude = _alt;

        geographic_msgs::KeyValue velo;
        velo.key = "velocity";
        velo.value = doubleToStr( _vel );
        set_gps_srv.request.target.props.push_back( velo );
        //call the service
        //set waypoint
        client_setgpstarget.call( set_gps_srv );
        //set to auto
        ardrone_autonomy::RecordEnable set_auto_srv;
        set_auto_srv.request.enable = true;
        client_setautoflight.call( set_auto_srv );

        if( set_auto_srv.response.result
         && set_gps_srv.response.result ){
            return true;
        }
        else{
            return false;
        }
    }
    else{
        YCLOG( s_logger, LL_DEBUG, "no gps, no waypoint sending");
        return false;
    }
}

int ArdroneGPS::PredictColli()
{
    int result = 0;
    if( wp_idx == -1 )
    {
        result = -1;
    }
    else
    {
        //UserTypes::NavigatorSim navigator;
        //navigator.SetHVThres( this->h_thres, this->v_thres );
        UserTypes::StateSim st_now = uav_st;
        for( int i = wp_idx; i >= 0; --i )
        {
            UserStructs::WaypointSim wp_sim = Utils::WpToSim( waypoints[i] );
            if( predict_nav.PropWpCheck( st_now,
                                       wp_sim,
                                       dt,
                                       obstacles,
                                       space_limit)
                    )
            {
                result = 1;
                break;
            }
            st_now = predict_nav.GetStNow();
        }
    }
    YCLOG( s_logger,
           LL_DEBUG,
           "predict collision:"
           << result );
    return result;
}

bool ArdroneGPS::SendWpTest( const double _lat,
                             const double _lon,
                             const double _alt,
                             const double _vel )
{
    if( (int)nav_gps.data_available > 0 )
    {
        YCLOG( s_logger, LL_DEBUG, "waypoint initial send check gps:"
               << (int)nav_gps.data_available );
        lat_s = _lat;
        lon_s = _lon;
        alt_s = _alt;
        Utils::ToUTM( lon_s, lat_s, x_take, y_take );

        YCLOG( s_logger, LL_DEBUG,
               std::setprecision(6) << std::fixed
               << "current lat:" << nav_gps.latitude << ' '
               << "current lon:" << nav_gps.longitude << ' '
               << "go lat:" << lat_s << ' '
               << "go lon:" << lon_s );

        if( SendWaypoint( _lat, _lon, _alt, _vel ) )
        {
            YCLOG( s_logger, LL_DEBUG, "a waypoint sent" );
            waypoints.push_back( UserStructs::Waypoint(lat_s,lon_s,alt_s,_vel) );
            wp_idx = 0;
            return true;
        }
        else{
            return false;
        }
    }
    else
    {
        YCLOG( s_logger, LL_DEBUG, "no gps, no go");
        return false;
    }
}

bool ArdroneGPS::InsertWpTest(const double _lat,
                              const double _lon,
                              const double _alt,
                              const double _vel)
{
    if( (int)nav_gps.data_available > 0 )
    {
        YCLOG( s_logger, LL_DEBUG, "waypoint inser check gps:"
               << (int)nav_gps.data_available );
        lat_s = _lat;
        lon_s = _lon;
        alt_s = _alt;
        Utils::ToUTM( lon_s, lat_s, x_take, y_take );

        YCLOG( s_logger, LL_DEBUG,
               std::setprecision(6) << std::fixed
               << "current lat:" << nav_gps.latitude << ' '
               << "current lon:" << nav_gps.longitude << ' '
               << "go lat:" << lat_s << ' '
               << "go lon:" << lon_s );

        if( SendWaypoint( _lat, _lon, _alt, _vel ) )
        {
            YCLOG( s_logger, LL_DEBUG, "a waypoint inserted" );
            if( waypoints.size() == 1 ){
                waypoints.push_back( UserStructs::Waypoint(lat_s,lon_s,alt_s,_vel) );

            }
            else if( waypoints.size() == 2 ){
                waypoints[1] = UserStructs::Waypoint( lat_s, lon_s, alt_s, _vel );
            }
            else{
                YCLOG( s_logger, LL_DEBUG, "wrong waypoints size" );
            }
            wp_idx = 1;
            return true;
        }
        else{
            return false;
        }
    }
    else
    {
        YCLOG( s_logger, LL_DEBUG, "no gps, no insert" );
        return false;
    }
}

void ArdroneGPS::CheckWpNav()
{
    if( (int)nav_gps.data_available > 0 )
    {
        if( lat_s != 0.
          && lon_s != 0.
          && alt_s != 0.)
        {
            double x_s, y_s;
            Utils::ToUTM( lon_s, lat_s, x_s, y_s );
            double wp_dis = std::sqrt( std::pow( x_s - uav_st.x, 2 )
                                  + std::pow( y_s - uav_st.y, 2 ) );

            double take_dis = std::sqrt( std::pow( x_take - uav_st.x, 2 )
                                  + std::pow( y_take - uav_st.y, 2 ) );
            YCLOG( s_logger, LL_DEBUG,
                   "wp_dis:" << wp_dis << ' '
                   << "take_dis:" << take_dis);

            if( nav_gps.X_ref != 0 &&
                nav_gps.X_ref == nav_gps.X_traj &&
                nav_gps.Y_ref != 0 &&
                nav_gps.Y_ref == nav_gps.Y_traj &&
                take_dis > 2.0
               )
            {
                if( uav_state_idx != 2 && uav_state_idx != 8 )
                {
                    if( wp_idx == 0 ){
                      sendStop();
                      sendLand();
                      YCLOG( s_logger, LL_DEBUG, "arrive, land");
                      wp_idx = -1;
                    }
                    else if( wp_idx == 1 ){
                      YCLOG( s_logger, LL_DEBUG, "inter wp arrived, go to the original one");
                      wp_idx = 0;

                      double lat_w = waypoints[0].lat;
                      double lon_w = waypoints[0].lon;
                      //bent back

                      double x_w, y_w;
                      Utils::ToUTM( lon_w, lat_w, x_w, y_w );
                      x_w -= 0;
                      Utils::FromUTM( x_w, y_w, lon_w, lat_w );

                      SendWaypoint( lat_w,
                                    lon_w,
                                    waypoints[0].alt,
                                    waypoints[0].speed );
                      t_transit = ros::Time::now();
                    }
                    else{
                      YCLOG( s_logger, LL_DEBUG, "wrong waypoint sequence");
                    }

                    YCLOG( s_logger, LL_DEBUG, "arrive refs:" << '\n'
                           << "X_ref:" << nav_gps.X_ref << ' '
                           << "X_traj:" << nav_gps.X_traj << ' '
                           << "Y_ref:" << nav_gps.Y_ref << ' '
                           << "Y_traj:" << nav_gps.Y_traj );

                }
            }

        }
    }
    else{
        YCLOG( s_logger, LL_DEBUG, "no gps, no waypoint check" );
    }
}

void ArdroneGPS::SetLogFileName(const char *filename)
{
    try
    {
        traj_log.exceptions ( std::ofstream::failbit | std::ofstream::badbit );
        traj_log.open(filename,std::ofstream::out
                      | std::ofstream::in
                      | std::ofstream::trunc);
    }
    catch (std::ofstream::failure& e) {
        std::cerr << "Exception opening/reading file"
                  << e.what()
                  << std::endl;
    }
}

void ArdroneGPS::SetObsDisFile(const char *filename)
{
    try
    {
        obdis_log.exceptions(std::ofstream::failbit | std::ofstream::badbit);
        obdis_log.open(filename,std::ofstream::out
                       | std::ofstream::in
                       | std::ofstream::trunc);
    }
    catch(std::ofstream::failure& e) {
        std::cerr << "Exception opening/reading file"
                  << e.what()
                  << std::endl;
    }
}

void ArdroneGPS::SetAvoidWpFile(const char *filename)
{
    try
    {
        avoid_wp_log.exceptions(std::ofstream::failbit | std::ofstream::badbit);
        avoid_wp_log.open(filename,std::ofstream::out
                       | std::ofstream::in
                       | std::ofstream::trunc);
    }
    catch(std::ofstream::failure& e) {
        std::cerr << "Exception opening/reading file"
                  << e.what()
                  << std::endl;
    }
}

std::string ArdroneGPS::doubleToStr( const double val )
{
    std::ostringstream os;
    os << val;
    return os.str();
}

}//namespace ends
