close all;
clear all;
clc;

%f_traj =fopen('/home/yucong/catkin_ws/devel/lib/ardrone_gps/obs_traj.txt','r');
f_traj =fopen('/home/yucong/catkin_ws/src/ardrone_gps/records/obs_traj.txt','r');

if f_traj == -1
    error('File traj_log.txt could not be opened, check name or path.')
end

traj_line= fgetl(f_traj);
sim_traj = [];

count = 1;
% x0 = 416878.740330;
% y0 = 3697125.599913;
while ischar(traj_line)
%1440980152.890608 33.410139 -111.893931 416879.891995 3697113.526170 360.340000 2.190000 0.229869 -0.079203 23.740421 23.740421 1.087235 1.087235 1.983929 0.253034 3
    log_traj = textscan(traj_line,'%f %f %f %f %f %f %f %f %f');
    t = log_traj{1};
    
    if count == 1
       t0 = t;
    end
    t = t - t0;
    
    lat = log_traj{2};
    lon = log_traj{3};
    
    x = log_traj{4};
    y = log_traj{5};
    if count == 1
       x0 = x;
       y0 = y;
    end
    x = x - x0;
    y = y - y0;
    
    alt = log_traj{6};
    speed = log_traj{7};
    vz = log_traj{8};
    yaw = log_traj{9} * 180. / pi;
    
    sim_traj = [ sim_traj; [t,lat,lon,x,y,alt,speed,vz,yaw] ]; 
    
    traj_line = fgetl( f_traj );
    count = count + 1;
    if count == 3000
       break;
    end
end

figure;
hold on;
axis equal;
grid on;
title('x-y');
plot( sim_traj(:,4), sim_traj(:,5), 'b*' );

figure;
hold on;
axis auto;
grid on;
title('yaw');
plot( sim_traj(:,1), sim_traj(:,9), 'b*' );

figure;
hold on;
axis auto;
grid on;
title('alt');
plot( sim_traj(:,1), sim_traj(:,6), 'b*' );