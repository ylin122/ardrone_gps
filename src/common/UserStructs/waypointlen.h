#ifndef WAYPOINTLEN_H
#define WAYPOINTLEN_H

#include "waypoint.h"

namespace UserStructs{

struct WaypointLen{
    Waypoint wp;
    double length;

    WaypointLen( Waypoint _wp,
                 double _len ):
               wp( _wp ),
               length( _len )
    {

    }
};

}

#endif // WAYPOINTLEN_H

