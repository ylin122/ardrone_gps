function [  ] = drawTriangle( x, y, Angle, s )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    Angle = Angle * pi / 180. - pi / 2;
    OriginX = 0;
    OriginY = 0;
    SideLen = 0.5;
    Height = 4*SideLen/2;

    TCoX = OriginX;
    TCoY = OriginY - Height/2;

    LCoX = OriginX - SideLen/2;
    LCoY = OriginY + Height/2;

    RCoX = OriginX + SideLen/2;
    RCoY = OriginY + Height/2;
    
    TCoY = - TCoY;
    LCoY = - LCoY;
    RCoY = - RCoY;
    
    NewCoT = [cos(Angle), - sin(Angle); sin(Angle), cos(Angle)]*[TCoX; TCoY] + [x; y];
    NewCoL = [cos(Angle), - sin(Angle); sin(Angle), cos(Angle)]*[LCoX; LCoY] + [x; y];
    NewCoR = [cos(Angle), - sin(Angle); sin(Angle), cos(Angle)]*[RCoX; RCoY] + [x; y];
    
    hold on;
    line([NewCoT(1) NewCoL(1)], [NewCoT(2) NewCoL(2)], 'Color', s );
    line([NewCoT(1) NewCoR(1)], [NewCoT(2) NewCoR(2)], 'Color', s );
    line([NewCoR(1) NewCoL(1)], [NewCoR(2) NewCoL(2)], 'Color', s );

end

