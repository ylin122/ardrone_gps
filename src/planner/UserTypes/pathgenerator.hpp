#ifndef PATHGENERATOR_HPP
#define PATHGENERATOR_HPP

#include "navigatorsim.hpp"
#include "statesim.hpp"
#include "common/UserStructs/waypointlen.h"
#include "common/UserTypes/spacelimit.h"
#include "planner/UserStructs/obstacle.h"
#include "common/UserTypes/polesampler.hpp"

#include "ros/ros.h"


namespace UserTypes{
  class PathGenerator{

  public:
     PathGenerator();
     ~PathGenerator(){ }
     //set path
     void SetPathLogName( const char* filename );

     inline void SetIfPathLog( const bool _if ){
         if_path_log = _if;
     }

     inline void SetTimeLimit( const double _t_limit ){
         this -> t_limit = _t_limit;
     }

     inline void SetSpaceLimit( UserTypes::SpaceLimit space_limit ){
         this -> space_limit = space_limit;
     }

     inline void SetObss( std::vector< UserStructs::obstacle > _obss ){
         this -> obstacles = _obss;
     }

     inline void SetIfInRos( const bool _if )
     {
         this -> if_in_ros = _if;
     }

     inline void SetCurrentSt( StateSim st_current )
     {
         st_start = st_current;
     }

     inline void SetGoalWp( UserStructs::WaypointSim wp )
     {
         wp_goal = wp;
     }

     inline void SetWpConfig( const double _speed,
                              const double _x_err,
                              const double _y_err,
                              const double _alt_err )
     {
         this -> wp_speed = _speed;
         this -> x_err = _x_err;
         this -> y_err = _y_err;
         this -> alt_err = _alt_err;
     }

     //generating potential paths
     int AddPaths();
     //check paths
     bool PathCheckRepeat( StateSim st_current );
     //return an intermediate waypoint
     inline UserStructs::Waypoint GetInterWp( ){
         return wp_inter;
     }

     //set h_thres and v_thres for navigator
     inline void SetHVThres( double _h_thres,
                             double _v_thres )
     {
         navigator.SetHVThres( _h_thres, _v_thres );
     }

  private:
     //navigator
     NavigatorSim navigator;
     //current state
     UserTypes::StateSim st_start;
     //goal
     UserStructs::WaypointSim wp_goal;
     //intermediate waypoint
     UserStructs::Waypoint wp_inter;
     //time limit for planning
     double t_limit;
     //ros::Time t_start;
     //double sec_count;
     //path log
     std::ofstream path_log;
     bool if_path_log;
     //if in ros or not
     bool if_in_ros;
     //records for all pathsP
     std::vector< UserStructs::WaypointLen > wp_lengths;
     //obstacles
     std::vector< UserStructs::obstacle > obstacles;
     //GeoFence
     UserTypes::SpaceLimit space_limit;
     //sampler
     UserTypes::PoleSampler sampler;
     //for path storage
     std::vector< UserTypes::StateSim > path_rec;
     //for inter wp
     double wp_speed;
     double x_err;
     double y_err;
     double alt_err;
     //for propagation
     double dt;
     //functions
     UserStructs::WaypointSim SamplePt();
  };
}

#endif // PATHGENERATOR_HPP

