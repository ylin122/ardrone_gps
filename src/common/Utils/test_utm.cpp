#include "utmtransform.h"
#include <iostream>
#include <iomanip>

int main( int argc, char** argv )
{
    double lat0 = 33.41011;
    double lon0 = -111.89398;

    double lat_t = 33.4100931;
    double lon_t = -111.8936773;

    double x0, y0, x_t, y_t;
    Utils::ToUTM( lon0, lat0, x0, y0 );
    Utils::ToUTM( lon_t, lat_t, x_t, y_t );
    //std::cout << "x_dis:" << x_t - x0 << '\n';
    //std::cout << "y_dis:" << y_t - y0 << '\n';
    double lat_s = 33.4102476; //north
    double lon_s = -111.8939441;
    double x_s, y_s;
    Utils::ToUTM( lon_s, lat_s, x_s, y_s );
    std::cout << std::setprecision(6) << std::fixed
              << x_s << ' ' << y_s << '\n';

}
