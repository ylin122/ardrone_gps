#include "ardronegps.hpp"
#include "common/Utils/yclogger.h"
#include "common/Utils/findpath.h"
#include "nodes/obsupdater/obsrealupdater.hpp"
#include "nodes/obsupdater/obsvirtualupdater.hpp"
#include "ros/ros.h"

int main( int argc, char** argv )
{
    ros::init( argc, argv, "InvaderGPS" );
    Nodes::ArdroneGPS ardrone_gps;

    Utils::LogConfigurator myconfigurator( "log4cxx_InvaderGPS.properties","log for the invader ardrone" );

    std::string traj_file = Utils::FindPath() + "records/traj_log.txt";
    ardrone_gps.SetLogFileName( traj_file.c_str() );

    std::string obdis_file = Utils::FindPath() + "records/obdis_log.txt";
    ardrone_gps.SetObsDisFile( obdis_file.c_str() );
    //single test: (0,0), as the invader (1,1)
    //for real obstacle
    auto monitor = std::unique_ptr< UserTypes::OthersMonitor > ( new UserTypes::OthersMonitor( 1, 1 ) );
    ardrone_gps.SetOthersMonitor( std::move( monitor ) );
    //obstacle updater
    auto obs_updater = std::unique_ptr< UserTypes::ObsUpdater >( new UserTypes::ObsRealUpdater(1,1) );
    //single test
    //auto obs_updater = std::unique_ptr< UserTypes::ObsUpdater >( new UserTypes::ObsRealUpdater( 0, 0 ) );
    ardrone_gps.SetObsUpdater( std::move( obs_updater ) );
    double spd = 1.0;
    double hgt = 1.5;
    ardrone_gps.working3( spd, hgt );
}
