#include "obsvirtualupdater.hpp"
#include "common/Utils/gettimenow.h"
#include "common/Utils/yclogger.h"
#include <fstream>

namespace{
Utils::LoggerPtr s_logger(Utils::getLogger("ardrone_gps.ObsVirtualUpdater.YcLogger"));
}

namespace UserTypes{

  ObsVirtualUpdater::ObsVirtualUpdater(std::vector<std::string> filenames):obs_count( 0 )
  {
      this -> ReadObss( filenames );
  }

  void ObsVirtualUpdater::ReadObss( std::vector< std::string >& filenames )
  {
      vec_obss.clear();
      /*
      << st_now.t << ' '
      << st_now.lat << ' '
      << st_now.lon << ' '
      << st_now.x << ' '
      << st_now.y << ' '
      << st_now.alt << ' '
      << st_now.speed << ' '
      << st_now.vz << ' '
      << st_now.yaw << '\n'; */
      for( int i = 0; i != filenames.size(); ++i )
      {
         std::vector< UserStructs::obstacle > obss;
         std::ifstream file( filenames[i].c_str() );
         while( file.good() )
         {
             UserStructs::obstacle obs;
             double lat, lon;
             file >> obs.t
                  >> lat
                  >> lon
                  >> obs.x
                  >> obs.y
                  >> obs.z
                  >> obs.speed
                  >> obs.vz
                  >> obs.heading;
             obss.push_back( obs );
         }
         vec_obss.push_back( obss );
      }
  }

  void ObsVirtualUpdater::UpdateObs(std::vector<UserStructs::obstacle>& obs3ds )
  {
      obs3ds.clear();
      for( int i = 0; i != vec_obss.size(); ++i )
      {
          if( obs_count > vec_obss[i].size() - 1 )
          {
              int end = vec_obss[i].size() - 1;
              double dt = obs_count - end;
              dt *= 0.1;
              UserStructs::obstacle obs = vec_obss[i][ end ].extropolate( dt );
              YCLOG( s_logger,
                     LL_DEBUG,
                     "obs: " << "x:" << obs.x
                             << "y:" << obs.y);
              obs.t = Utils::GetTimeNow();
              obs3ds.push_back( obs );
          }
          else
          {
              UserStructs::obstacle obs = vec_obss[i][obs_count];
              obs.t = Utils::GetTimeNow();
              obs3ds.push_back( obs );
          }
      }
      YCLOG( s_logger,
             LL_DEBUG,
             "obs_count:" << obs_count );
      ++ obs_count;
  }

}
