#include "othersmonitor.hpp"
#include "common/Utils/yclogger.h"

#include "ros/ros.h"
#include "std_msgs/Bool.h"
#include "boost/bind.hpp"
#include <stdexcept>

namespace{
Utils::LoggerPtr s_logger(Utils::getLogger("ardrone_gps.OthersMonitor.YcLogger"));
}

namespace UserTypes
{
  OthersMonitor::OthersMonitor(int _one,int _total):one(_one),total(_total)
  {
    if( total < 0 )
      try{
         throw std::runtime_error ("the argument total must be no less than 0");
      }
         catch (std::runtime_error &e) {
          std::cout << "Caught a runtime_error exception: "
               << e.what () << '\n';
      }
    //initialize

    for(int i =0 ; i != total + 1; ++i )
    {
      int idx = i;
      std::ostringstream convert;
      convert << idx;
      std::string idx_str = convert.str();

      if_take_offs.push_back( false );
      //subscriber to if take off
      ros::Subscriber sub_off = nh.subscribe<std_msgs::Bool>(std::string("/drone")+idx_str+"/if_take_off", 1, boost::bind( &OthersMonitor::offCb, this, _1, idx ) );
      take_off_subs.push_back( sub_off);

      YCLOG( s_logger ,LL_DEBUG, "set if_stables "
             << i << ' '
             << "false");
      if_stables.push_back(false);
      //subscriber to if stable
      ros::Subscriber sub_stable = nh.subscribe<std_msgs::Bool>(std::string("/drone")+idx_str+"/if_stable",1,boost::bind(&OthersMonitor::stableCb, this, _1, idx) );
      stable_subs.push_back( sub_stable );

    }//for ends

  }//OthersMonitor ends

  bool OthersMonitor::ifOthersTakeOff()
  {
      if( total == 0 ){
          return true;
      }
      for( int i = 0; i != total + 1; ++i )
      {
          if(i == one){
              continue;
          }
          if( if_take_offs[i] == false ){
              return false;
          }
      }
      return true;
  }//ifOthersTakeOff ends

  bool OthersMonitor::ifOthersStable()
  {
      if( total == 0 ){
          return true;
      }
      for(int i = 0; i != total + 1; ++i )
      {
          YCLOG( s_logger,
                 LL_DEBUG,
                 "IfOther:"
                 << i << ' '
                 << this -> if_stables[i]
                 );

          if( i == one ){
              continue;
          }
          if( !this -> if_stables[i] ){
              YCLOG( s_logger,
                     LL_DEBUG,
                     "i:" << i << ","
                     << "other not statble yet");
              return false;
          }
      }
      YCLOG( s_logger, LL_DEBUG, "other stable");
      return true;
  }//ifOthersStable ends


  bool OthersMonitor::ifSomeTakeOff(int _idx)
  {
      if( total == 0 ) {
          return true;
      }
      if( _idx == one )
      {
          try {
              throw std::runtime_error ("the one to inspect shouldn't be the host one");
          }
          catch (std::runtime_error &e) {
              std::cout << "Caught a runtime_error exception: "
                        << e.what () << '\n';
          }
      }
      //
      if( _idx < one ){
          return if_take_offs[_idx];
      }
      return if_take_offs[_idx-1];
  }//ifSomeTakeOff ends

  //call back functions
  void OthersMonitor::offCb(const std_msgs::Bool::ConstPtr& msg, int _idx)
  {
      if_take_offs[_idx]= msg->data;
  }//offCb ends

  void OthersMonitor::stableCb(const std_msgs::Bool::ConstPtr& msg, int _idx)
  {

      this -> if_stables[_idx]= msg->data;
      YCLOG( s_logger,
             LL_DEBUG,
             "idx:" << _idx << ' '
             << "if_stable:" << this -> if_stables[ _idx ] );
  }



}
