#include "findpath.h"
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

namespace Utils{
 std::string FindPath()
 {
     std::string path="/home/yucong/catkin_ws/src/ardrone_gps/";
     fs::path p1(path);

     if( fs::exists(p1) )
     {
        return path;
     }
     else
     {
        return "/home/srik/catkin_ws/src/ardrone_gps/";
     }
 }

}
