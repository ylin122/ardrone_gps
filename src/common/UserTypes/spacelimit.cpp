#include "spacelimit.h"
#include "common/Utils/pointinpoly.h"
#include "common/Utils/yclogger.h"

#include "stdexcept"
#include <fstream>
#include <sstream>
#include <iostream>

namespace{
Utils::LoggerPtr s_logger(Utils::getLogger("ardrone_gps.SpaceLimit.YcLogger"));
}

namespace UserTypes{

 SpaceLimit::SpaceLimit():
     h_upper(0.),
     h_lower(0.)
 {

 }

 SpaceLimit::SpaceLimit( double _h_upper,
                         double _h_lower ):
                       h_upper( _h_upper ),
                       h_lower( _h_lower )
 {

 }

 bool SpaceLimit::TellIn( double x,
                          double y,
                          double z )
 {
     if( z <= h_lower || z >= h_upper ){
         YCLOG( s_logger, LL_DEBUG,
                "height out" << '  '
                << "upper:" << h_upper << ' '
                << "lower:" << h_lower << ' '
                << "z:" << z << '\n' );
         return false;
     }

     if( !Utils::PointInPoly( vertex, x, y ) ){
         //YCLOG( s_logger, LL_DEBUG, "fence out" );
         return false;
     }
     return true;

 }//TellIn ends

void SpaceLimit::LoadGeoFence( const char* filename )
{
    std::fstream myfile(filename);
    if( myfile.is_open() )
    {
        std::string line;
        while(std::getline(myfile,line) )
        {
            std::istringstream iss(line);
            double lat,lon,x,y;
            iss >> lat >> lon;
            Utils::ToUTM(lon,lat,x,y);
            vertex.push_back(UserStructs::point2D(x,y) );
        } //while ends
    }
    else
    {
        try {
            throw std::runtime_error("error: geofence file not found");
        }
        catch ( std::runtime_error &e ){
            std::cout << "Caught a runtime_error exception:" << e.what() << '\n';
        }
    }

}

}
