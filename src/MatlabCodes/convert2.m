close all;
h = open('y_comp.fig');

%set(gca,'fontsize',20);
ti = get(gca,'TightInset')
cur = 0.05
set(gca,'Position',[ti(1)+cur ti(2)+cur 1-ti(3)-ti(1)+0.3 1-ti(4)-ti(2)+0.4]);

set(gca,'units','centimeters')
pos = get(gca,'Position');
ti = get(gca,'TightInset');

set(gcf, 'PaperUnits','centimeters');
set(gcf, 'PaperSize', [pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)+2]);
set(gcf, 'PaperPositionMode', 'manual');

cur = 0;
set(gcf, 'PaperPosition',[0+cur 0+cur pos(3)+ti(1)+ti(3)-cur pos(4)+ti(2)+ti(4)+8]);
%set(gcf,'Units','normal');
%set(gca,'Position',[0 0 1 1])
% saveTightFigure(h,'y_wide.fig');

print -painters -dpdf  y_comp.pdf