close all;

%load uav trajectory
f_traj =fopen('../../records/traj_log_20150912-090404.txt','r');

if f_traj == -1
    error('File traj_log.txt could not be opened, check name or path.')
end

traj_line= fgetl(f_traj);
virtual_traj = [];
count = 1;
x0 = 0;
y0 = 0;
t0 = 0;
while ischar(traj_line)
%1440980152.890608 33.410139 -111.893931 416879.891995 3697113.526170 360.340000 2.190000 0.229869 -0.079203 23.740421 23.740421 1.087235 1.087235 1.983929 0.253034 3
    log_traj = textscan(traj_line,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f');
    state_id = log_traj{16};

    if state_id == 3
    
        t = log_traj{1};

        if count == 1
           t0 = t;
        end
        t = t - t0;

        lat = log_traj{2};
        lon = log_traj{3};

        x = log_traj{4};
        y = log_traj{5};
        if count == 1
           x0 = x;
           y0 = y;
        end

        x = x - x0;
        
        if x < 0
           x = x * 0.2; 
        end
        
        y = y - y0;

        alt = log_traj{6};
        speed = log_traj{7};
        vz = log_traj{8};
        yaw = log_traj{9} * 180. / pi;
        X_ref = log_traj{10};
        X_traj = log_traj{11};
        Y_ref = log_traj{12};
        Y_traj = log_traj{13};
        vx_traj = log_traj{14};
        vy_traj = log_traj{15};

        virtual_traj = [ virtual_traj; [t,lat,lon,x,y,alt,speed,vz,yaw,X_ref,X_traj,Y_ref,Y_traj,vx_traj,vy_traj,state_id] ];
        
        count = count + 1;
    end
    traj_line = fgetl( f_traj );
    
end

%load obs trajectory
f_obs =fopen('../../records/obs_20150912-090404.txt','r');

if f_obs == -1
    error('File obs_log.txt could not be opened, check name or path.')
end

obs_line= fgetl(f_obs);
obs_traj = [];
start = 0;
while ischar(obs_line)
    %0 1442073860.223958 416877.862080 3697119.708592 0.702000 -85.927002 0.100000 29.438983 0.041000
    %0 t:1442073860.223958 x:416877.862080 y:3697119.708592 z:0.702000 heading:-85.927002 speed:0.100000 dis_h:29.438983 dis_v:0.041000
    log_obs = textscan(obs_line,'%f %f %f %f %f %f %f %f %f');
    
    t = log_obs{2};
    if abs( t - t0 ) < 0.1
        start = 1;
    end
    
    if start == 1
       t = log_obs{2} - t0;
       x = log_obs{3} - x0;
       
       if x > 0
          x = x * 0.2; 
       end
       
       y = log_obs{4} - y0;
       z = log_obs{5};
       hd = log_obs{6};
       obs_traj = [ obs_traj; [t,x,y,z,hd] ];
    end
    
    obs_line = fgetl( f_obs );
end

%plot the trajectory
h = figure;
hold on;
axis equal;
ylim([-1 3])
xlim([-2 16])
grid on;
xlabel('north(m)');
ylabel('east(m)');

ed = 120;
plot( virtual_traj(1:ed,5), virtual_traj(1:ed,4), 'b-' );
plot( obs_traj(1:ed,3), obs_traj(1:ed,2), 'r-' );
legend('host','obstacle','Location','northeast');
legend boxoff;
%drawTriangle( 0, 0, 0, 'r' );

%plot triangles
inc = 30;
count = 0;
tt_dis = 0.6;
for i = 1 : ed
   if mod( i,  inc ) == 0 || i == 1 || i == ed
       
       x_uav = virtual_traj( i, 4 );
       y_uav = virtual_traj( i, 5 );
       hd_uav = virtual_traj( i, 9 );
       drawTriangle( y_uav, x_uav, 90 - hd_uav, 'b' );
       text( y_uav, x_uav - tt_dis, num2str( count ), 'Color', 'b' );
%        drawTriangle( x_uav, y_uav, hd_uav, 'b' );
%        text( x_uav, y_uav-0.3, num2str( count ), 'Color', 'b' );
       
       
       x_obs = obs_traj( i, 2 );
       y_obs = obs_traj( i, 3 );
       hd_obs = obs_traj( i, 5 );
       drawTriangle( y_obs, x_obs, 90 - hd_obs, 'r' );
       text( y_obs, x_obs + tt_dis, num2str( count ), 'Color', 'r' );
%        drawTriangle( x_obs, y_obs, hd_obs, 'r' );
%        text( x_obs, y_obs + 0.3, num2str( count ), 'Color', 'r' );
       count = count + 1;
   end
end

saveas(h,'avoid_traj','fig');